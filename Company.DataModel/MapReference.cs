﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.DataModel
{
    /// <summary>
    ///     This class's only purpose is to provide a reference to assembly with class mapping for fluent mapper.
    /// </summary>
    public sealed class MapReference
    {
    }
}
