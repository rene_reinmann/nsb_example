﻿using System;
using Company.Persitence.NHibernate;

namespace Company.DataModel.Entity
{
    public class TestObjectRequestDebt : PersistentObjectWithTypedId<Guid>
    {
        public TestObjectRequestDebt(TestObjectRequest testObjectRequest)
        {
            TestObjectRequest = testObjectRequest;
        }

        protected TestObjectRequestDebt()
        {

        }

        public virtual TestObjectRequest TestObjectRequest { get; set; }

        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Amount { get; set; }
        public virtual string MaxAmount { get; set; }
        public virtual string Industry { get; set; }
        public virtual string Reporter { get; set; }

        protected override string GetDomainObjectSignature()
        {
            return GetType() + "|" + ID;
        }
    }
}