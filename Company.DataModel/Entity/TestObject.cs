﻿using Company.Persitence.NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Company.DataModel.Entity
{
    public class TestObject : PersistentObjectWithTypedId<Guid>
    {
        public virtual int Version { get; set; }

        public TestObject(TestObjectRequest request)
        {
            SetAssignedIdTo(request.ID);

            CreatedOn = DateTime.Now;

            Country = request.Country;
            PartyId = request.PartyId;
            Length = request.Duration;
            Amount = request.AppliedAmount;
            Interest = request.Interest;

            TestObjectStatuses = new List<TestObjectStatus>();

            ChangeStatusTo(TestObjectStatusCode.Created);
        }


        protected TestObject()
        {

        }
        public virtual DateTime CreatedOn { get; set; }
        public virtual string Country { get; set; }
        public virtual Guid PartyId { get; set; }
        public virtual int Length { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal Interest { get; set; }

        public virtual IList<TestObjectStatus> TestObjectStatuses { get; set; }

        public virtual void ChangeStatusTo(TestObjectStatusCode statusCode, DateTime? activeFromDate = null)
        {
            var status = new TestObjectStatus(this, statusCode, activeFromDate);
            var current = GetCurrentStatus();
            if (current != null)
            {
                if (current.ActiveToDate == null)
                {
                    current.ActiveToDate = status.ActiveFromDate;
                }
            }

            TestObjectStatuses.Add(status);
        }

        public virtual TestObjectStatus GetCurrentStatus()
        {
            DateTime timeNow = DateTime.Now;
            TestObjectStatus result = TestObjectStatuses.FirstOrDefault(ls => ls.ActiveFromDate <= timeNow && (ls.ActiveToDate == null || ls.ActiveToDate > timeNow));

            return result;
        }

        public virtual void SetAssignedIdTo(Guid assignedId)
        {
            ID = assignedId;
        }

        protected override string GetDomainObjectSignature()
        {
            return GetType() + "|" + ID;
        }
    }
}