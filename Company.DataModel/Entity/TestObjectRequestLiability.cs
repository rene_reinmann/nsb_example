﻿using System;
using Company.Persitence.NHibernate;

namespace Company.DataModel.Entity
{
    public class TestObjectRequestLiability : PersistentObjectWithTypedId<Guid>
    {
        public TestObjectRequestLiability(TestObjectRequest testObjectRequest)
        {
            TestObjectRequest = testObjectRequest;
        }

        protected TestObjectRequestLiability()
        {

        }

        public virtual TestObjectRequest TestObjectRequest { get; set; }

        public virtual string CreditorName { get; set; }
        public virtual decimal MonthlyPaymentAmount { get; set; }
        public virtual decimal? OutstandingAmount { get; set; }
        public virtual int CollateralType { get; set; }
        public virtual int LiabilityType { get; set; }
        public virtual bool WillBeRefinanced { get; set; }

        protected override string GetDomainObjectSignature()
        {
            return GetType() + "|" + ID;
        }
    }
}