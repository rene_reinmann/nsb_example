﻿using System;
using Company.Persitence.NHibernate;

namespace Company.DataModel.Entity
{
    [Serializable]
    public class TestObjectStatus : PersistentObjectWithTypedId<Guid>
    {
        #region Constructors

        public TestObjectStatus(TestObject testObject, TestObjectStatusCode statusCode, DateTime? activeFromDate = null)
        {
            ActiveFromDate = activeFromDate ?? DateTime.Now;
            TestObject = testObject;
            TestObjectStatusCode = statusCode;
        }

        protected TestObjectStatus()
        {
        }

        #endregion

        #region Public Properties

        public virtual DateTime ActiveFromDate { get; set; }

        public virtual DateTime? ActiveToDate { get; set; }

        public virtual TestObject TestObject { get; set; }

        public virtual TestObjectStatusCode TestObjectStatusCode { get; set; }

        #endregion

        protected override string GetDomainObjectSignature()
        {
            return GetType() + "|" + ID;
        }
    }
}