﻿namespace Company.DataModel.Entity
{
    public enum TestObjectStatusCode
    {
        Created = 0, // not signed
        Open = 1, // in auction
        Successful = 2, // in repayment
        Failed = 3, // cancelled
    }
}