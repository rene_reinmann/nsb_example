﻿using System;
using System.Collections.Generic;
using Company.Persitence.NHibernate;

namespace Company.DataModel.Entity
{
    public class TestObjectRequest : PersistentObjectWithTypedId<Guid>
    {
        public TestObjectRequest()
        {
        }
        public virtual int Number { get; set; }
        public virtual DateTime ApplicationStartedDate { get; set; }
        public virtual DateTime ApplicationSubmittedDate { get; set; }
        public virtual decimal AppliedAmount { get; set; }
        public virtual decimal Interest { get; set; }
        public virtual string Country { get; set; }
        public virtual int Duration { get; set; }
        public virtual int MonthlyPaymentDay { get; set; }
        public virtual decimal MonthlyPayment { get; set; }
        public virtual decimal MonthlyLivingExpenses { get; set; }
        public virtual decimal DebtToIncome { get; set; }
        public virtual decimal FreeCash { get; set; }
        public virtual decimal? AnnualPercentageRate { get; set; }
        public virtual Guid PartyId { get; set; }
        public virtual string Username { get; set; }
        public virtual string ReferralNr { get; set; }
        public virtual string AddressCountry { get; set; }
        public virtual string AddressCounty { get; set; }
        public virtual string AddressCity { get; set; }
        public virtual DateTime? DateOfBirth { get; set; }
        public virtual int Education { get; set; }
        public virtual string EmploymentDurationCurrentEmployer { get; set; }
        public virtual string EmploymentPosition { get; set; }
        public virtual int EmploymentStatus { get; set; }
        public virtual int Gender { get; set; }
        public virtual int HomeOwnershipType { get; set; }
        public virtual int LanguageCode { get; set; }
        public virtual int MaritalStatus { get; set; }
        public virtual string NrOfDependants { get; set; }
        public virtual int OccupationArea { get; set; }
        public virtual string WorkExperience { get; set; }
        public virtual int IncomeAndExpensesVerificationType { get; set; }
        public virtual decimal IncomeFromPrincipalEmployer { get; set; }
        public virtual decimal IncomeFromPension { get; set; }
        public virtual decimal IncomeFromFamilyAllowance { get; set; }
        public virtual decimal IncomeFromSocialWelfare { get; set; }
        public virtual decimal IncomeFromLeavePay { get; set; }
        public virtual decimal IncomeFromChildSupport { get; set; }
        public virtual decimal IncomeOther { get; set; }
        public virtual decimal IncomeTotal { get; set; }
        public virtual bool NewCreditCustomer { get; set; }
        public virtual string CreditScore { get; set; }
        public virtual string Rating { get; set; }
        public virtual string ModelVersion { get; set; }
        public virtual double ExpectedLoss { get; set; }
        public virtual double ExpectedReturn { get; set; }
        public virtual double LossGivenDefault { get; set; }
        public virtual double ExposureAtDefault { get; set; }
        public virtual double ProbabilityOfDefault { get; set; }
        public virtual double ProbabilityOfBad { get; set; }

        public virtual IList<TestObjectRequestLiability> Liabilities { get; set; }
        public virtual IList<TestObjectRequestDebt> Debts { get; set; }

        public virtual void SetAssignedIdTo(Guid assignedId)
        {
            ID = assignedId;
        }

        protected override string GetDomainObjectSignature()
        {
            return GetType() + "|" + ID;
        }
    }
}