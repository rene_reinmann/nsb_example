﻿using Company.DataModel.Entity;
using FluentNHibernate.Mapping;

namespace Company.DataModel.Mapping
{
    public class TestObjectRequestDebtMap : ClassMap<TestObjectRequestDebt>
    {
        public TestObjectRequestDebtMap()
        {
            Table("TestObjectRequestDebt");

            Id(x => x.ID).GeneratedBy.Guid();

            Map(x => x.StartDate);
            Map(x => x.EndDate);
            Map(x => x.Amount);
            Map(x => x.MaxAmount);
            Map(x => x.Industry);
            Map(x => x.Reporter);

            References(x => x.TestObjectRequest, "TestObjectId");
        }
    }
}