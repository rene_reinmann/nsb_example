﻿using Company.DataModel.Entity;
using FluentNHibernate.Mapping;

namespace Company.DataModel.Mapping
{
    public class TestObjectMap : ClassMap<TestObject>
    {
        public TestObjectMap()
        {
            DynamicInsert();
            DynamicUpdate();

            Table("TestObject");

            Id(x => x.ID).GeneratedBy.Assigned();

            Version(x => x.Version);

            Map(x => x.CreatedOn);
            Map(x => x.Country);
            Map(x => x.PartyId);
            Map(x => x.Length);
            Map(x => x.Amount);
            Map(x => x.Interest);

            HasMany(x => x.TestObjectStatuses).KeyColumn("TestObjectId").Inverse().Cascade.All().BatchSize(50);
        }
    }
}