﻿using Company.DataModel.Entity;
using FluentNHibernate.Mapping;

namespace Company.DataModel.Mapping
{
    public class TestObjectStatusMap : ClassMap<TestObjectStatus>
    {
        public TestObjectStatusMap()
        {
            DynamicInsert();
            DynamicUpdate();

            Table("TestObjectStatus");

            Id(x => x.ID).GeneratedBy.Guid();

            Map(x => x.ActiveFromDate);
            Map(x => x.ActiveToDate);
            Map(x => x.TestObjectStatusCode).CustomType<TestObjectStatusCode>();

            References(x => x.TestObject, "TestObjectId").Cascade.SaveUpdate();
        }
    }
}