﻿using Company.DataModel.Entity;
using FluentNHibernate.Mapping;

namespace Company.DataModel.Mapping
{
    public class TestObjectRequestMap : ClassMap<TestObjectRequest>
    {
        public TestObjectRequestMap()
        {
            Table("TestObjectRequest");

            Id(x => x.ID, "TestObjectId").GeneratedBy.Assigned();

            Map(x => x.ApplicationStartedDate);
            Map(x => x.ApplicationSubmittedDate);
            Map(x => x.Number);
            Map(x => x.AppliedAmount);
            Map(x => x.Interest);
            Map(x => x.Country);
            Map(x => x.Duration);
            Map(x => x.MonthlyPaymentDay);
            Map(x => x.MonthlyPayment);
            Map(x => x.MonthlyLivingExpenses);
            Map(x => x.DebtToIncome);
            Map(x => x.FreeCash);
            Map(x => x.AnnualPercentageRate);
            Map(x => x.PartyId);
            Map(x => x.Username);
            Map(x => x.ReferralNr);
            Map(x => x.AddressCountry);
            Map(x => x.AddressCounty);
            Map(x => x.AddressCity);
            Map(x => x.DateOfBirth);
            Map(x => x.Education);
            Map(x => x.EmploymentStatus);
            Map(x => x.EmploymentDurationCurrentEmployer);
            Map(x => x.EmploymentPosition);
            Map(x => x.Gender);
            Map(x => x.HomeOwnershipType);
            Map(x => x.LanguageCode);
            Map(x => x.MaritalStatus);
            Map(x => x.OccupationArea);
            Map(x => x.WorkExperience);
            Map(x => x.NrOfDependants);
            Map(x => x.IncomeAndExpensesVerificationType);
            Map(x => x.IncomeFromPrincipalEmployer);
            Map(x => x.IncomeFromPension);
            Map(x => x.IncomeFromFamilyAllowance);
            Map(x => x.IncomeFromSocialWelfare);
            Map(x => x.IncomeFromLeavePay);
            Map(x => x.IncomeFromChildSupport);
            Map(x => x.IncomeOther);
            Map(x => x.IncomeTotal);
            Map(x => x.NewCreditCustomer);
            Map(x => x.CreditScore);
            Map(x => x.Rating);
            Map(x => x.ModelVersion);
            Map(x => x.ExpectedLoss);
            Map(x => x.ExpectedReturn);
            Map(x => x.LossGivenDefault);
            Map(x => x.ProbabilityOfDefault);
            Map(x => x.ProbabilityOfBad);
            Map(x => x.ExposureAtDefault);

            HasMany(x => x.Liabilities).Inverse().Cascade.SaveUpdate();
            HasMany(x => x.Debts).Inverse().Cascade.SaveUpdate();
        }
    }
}