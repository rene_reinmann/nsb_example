﻿using Company.DataModel.Entity;
using FluentNHibernate.Mapping;

namespace Company.DataModel.Mapping
{
    public class TestObjectRequestLiabilityMap : ClassMap<TestObjectRequestLiability>
    {
        public TestObjectRequestLiabilityMap()
        {
            Table("TestObjectRequestLiability");

            Id(x => x.ID).GeneratedBy.Guid();

            Map(x => x.LiabilityType);
            Map(x => x.CreditorName);
            Map(x => x.MonthlyPaymentAmount);
            Map(x => x.OutstandingAmount);
            Map(x => x.CollateralType);
            Map(x => x.WillBeRefinanced);

            References(x => x.TestObjectRequest, "TestObjectId");
        }
    }
}