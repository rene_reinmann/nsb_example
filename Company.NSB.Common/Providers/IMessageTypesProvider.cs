﻿using System;
using System.Collections.Generic;

namespace Company.NSB.Common
{
    public interface IMessageTypesProvider
    {
        IEnumerable<Type> GetMessageTypes();
    }
}