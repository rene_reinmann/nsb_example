﻿using System.Collections.Generic;
using System.Reflection;

namespace Company.NSB.Common
{
    public interface IMessageAssembliesProvider
    {
        IEnumerable<Assembly> GetMessageAssemblies();
    }
}