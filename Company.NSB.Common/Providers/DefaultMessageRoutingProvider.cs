﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Company.NSB.Common
{
    public class DefaultMessageRoutingProvider : IMessageRoutingProvider
    {
        private readonly IReadOnlyCollection<IMessageRouting> _messageRouting;

        public DefaultMessageRoutingProvider(IMessageAssembliesProvider messageAssembliesProvider)
        {
            if (messageAssembliesProvider == null)
                throw new ArgumentNullException(nameof(messageAssembliesProvider));

            var messageRoutingTypes = messageAssembliesProvider.GetMessageAssemblies()
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => typeof(IMessageRoutingDefinitions).IsAssignableFrom(x));

            var messageRoutings = new List<IMessageRouting>();
            foreach (Type messageRoutingType in messageRoutingTypes)
            {
                var commandRoutings = messageRoutingType.GetCustomAttributes<CommandHandledByAttribute>(false);
                if (commandRoutings?.Count() > 0)
                    messageRoutings.AddRange(commandRoutings);

                var eventRoutings = messageRoutingType.GetCustomAttributes<EventPublishedByAttribute>(false);
                if (eventRoutings?.Count() > 0)
                    messageRoutings.AddRange(eventRoutings);
            }

            _messageRouting = messageRoutings.AsReadOnly();
        }

        public IReadOnlyCollection<IMessageRouting> GetMessageRouting()
        {
            return _messageRouting;
        }
    }

}