﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Company.NSB.Messages;
using Company.NSB.Messages.AssemblyAttributes;

namespace Company.NSB.Common
{
    public class DefaultMessageAssembliesProvider : IMessageAssembliesProvider
    {
        private readonly IReadOnlyCollection<Assembly> _assemblies;

        public DefaultMessageAssembliesProvider()
        {
            var result = new List<Assembly>();
            var queue = new Queue<Assembly>();
            var checkedAssemblies = new HashSet<string>();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                queue.Enqueue(assembly);

            while (queue.Count != 0)
            {
                Assembly assembly = queue.Dequeue();
                if (checkedAssemblies.Contains(assembly.FullName))
                    continue;

                checkedAssemblies.Add(assembly.FullName);

                foreach (AssemblyName referencedAssemblyName in assembly.GetReferencedAssemblies())
                {
                    if (checkedAssemblies.Contains(referencedAssemblyName.FullName))
                        continue;

                    try
                    {
                        queue.Enqueue(Assembly.Load(referencedAssemblyName));
                    }
                    catch
                    {
                        // ignored
                    }
                }

                if (assembly.GetCustomAttribute<MessageAssemblyAttribute>() == null)
                    continue;

                result.Add(assembly);
            }

            _assemblies = result.AsReadOnly();
        }

        public IEnumerable<Assembly> GetMessageAssemblies()
        {
            return _assemblies;
        }
    }

}