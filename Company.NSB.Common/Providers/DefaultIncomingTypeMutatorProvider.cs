﻿using System;
using System.Collections.Generic;
using System.Linq;
using Company.NSB.Messages.AssemblyAttributes;
using Company.NSB.Messages.Mutations;
using System.Reflection;

namespace Company.NSB.Common
{
    public class DefaultIncomingTypeMutatorProvider : IIncomingTypeMutatorProvider
    {
        private readonly IReadOnlyCollection<IMutateIncomingType> _mutators;

        public DefaultIncomingTypeMutatorProvider(IMessageAssembliesProvider messageAssembliesProvider)
        {
            if (messageAssembliesProvider == null)
                throw new ArgumentNullException(nameof(messageAssembliesProvider));

            _mutators = messageAssembliesProvider.GetMessageAssemblies()
                .SelectMany(x => x.GetCustomAttributes<MutateIncomingMessageAssemblyAttribute>())
                .Cast<IMutateIncomingType>()
                .ToList()
                .AsReadOnly();
        }

        public IEnumerable<IMutateIncomingType> GetIncomingTypeMutators()
        {
            return _mutators;
        }
    }
}