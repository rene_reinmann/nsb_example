﻿using System.Collections.Generic;
using Company.NSB.Messages.Mutations;

namespace Company.NSB.Common
{
    public interface IIncomingTypeMutatorProvider
    {
        IEnumerable<IMutateIncomingType> GetIncomingTypeMutators();
    }
}