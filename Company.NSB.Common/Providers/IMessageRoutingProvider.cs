﻿using System.Collections.Generic;

namespace Company.NSB.Common
{
    public interface IMessageRoutingProvider
    {
        IReadOnlyCollection<IMessageRouting> GetMessageRouting();
    }
}