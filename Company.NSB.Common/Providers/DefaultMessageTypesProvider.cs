﻿using System;
using System.Collections.Generic;
using System.Linq;
using Company.NSB.Messages;

namespace Company.NSB.Common
{
    public class DefaultMessageTypesProvider : IMessageTypesProvider
    {
        private readonly IReadOnlyCollection<Type> _messageTypes;

        public DefaultMessageTypesProvider(IMessageAssembliesProvider messageAssembliesProvider)
        {
            if (messageAssembliesProvider == null)
                throw new ArgumentNullException(nameof(messageAssembliesProvider));

            _messageTypes = messageAssembliesProvider.GetMessageAssemblies()
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => typeof(IBaseMessage).IsAssignableFrom(x))
                .ToList()
                .AsReadOnly();
        }

        public IEnumerable<Type> GetMessageTypes()
        {
            return _messageTypes;
        }
    }
}