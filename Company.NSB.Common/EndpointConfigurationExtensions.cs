﻿using Autofac;
using Company.Config;
using Company.NSB.Messages;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.MessageMutator;
using NServiceBus.Persistence.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Company.NSB.Common.Mutators;
using Company.Common.Logging;

namespace Company.NSB.Common
{
    public static class EndpointConfigurationExtensions
    {
        public static EndpointConfiguration UseDefaultConfiguration<TEndpoint, TMapping>(this EndpointConfiguration c, IEndpointConfig config)
            where TMapping : NsbLogicalRouting, new()
        {
            var transport = c
                .ConfigureLogging()
                .ConfigurePersistence()
                .ExcludeAllAssembliesExceptStartingWith<TEndpoint>("NServiceBus", "Company.NSB.Common")
                .ConfigureMessageConventions<TEndpoint>()
                .UseTransport<MsmqTransport>();

            var physicalRouting = NsbPhysicalRouting.Get();
            var routingSettings = transport.Routing();
            var routing = NsbLogicalRouting.Map<TMapping>(routingSettings);

            physicalRouting.EnsureRoutingExists(routing);

            c.SendFailedMessagesTo(physicalRouting.GetPhysicalAddress(config.ErrorQueue));

            if (config.AuditQueue != null)
                c.AuditProcessedMessagesTo(physicalRouting.GetPhysicalAddress(config.AuditQueue));
            
            string localQueueName = config.EndpointName ?? typeof(TEndpoint).Namespace;
            string publicQueueName = config.EndpointQueue ?? $"{localQueueName.ToLower()}@{Environment.MachineName}";

            c.RegisterComponents(components =>
            {
                if (!string.IsNullOrEmpty(localQueueName)
                    && !localQueueName.EndsWith("eventlog", StringComparison.OrdinalIgnoreCase)
                    && !localQueueName.EndsWith("eventerrorlog", StringComparison.OrdinalIgnoreCase))
                {
                    components.ConfigureComponent(() => new LogSupportMessageMutator(publicQueueName), DependencyLifecycle.SingleInstance);
                }

                components.ConfigureComponent<DefaultMessageAssembliesProvider>(DependencyLifecycle.SingleInstance);
                components.ConfigureComponent<DefaultMessageTypesProvider>(DependencyLifecycle.SingleInstance);
                components.ConfigureComponent<DefaultMessageRoutingProvider>(DependencyLifecycle.SingleInstance);
                components.ConfigureComponent<DefaultIncomingTypeMutatorProvider>(DependencyLifecycle.SingleInstance);
                components.ConfigureComponent<IncomingTypeMessageMutator>(DependencyLifecycle.SingleInstance);
            });

            c.RegisterMessageMutator(new ResponseMutator());

            if (config.UseJsonSerializer)
            {
                c.UseSerialization<NewtonsoftSerializer>();
                c.AddDeserializer<XmlSerializer>(); // Also support XML deserialization to support old format
            }

            if (Debugger.IsAttached || !config.MultithreadingEnabled)
                c.LimitMessageProcessingConcurrencyTo(1);
            else
                c.LimitMessageProcessingConcurrencyTo(config.MessageProcessingConcurrency);

            if (config.DisableDTC)
                transport.Transactions(TransportTransactionMode.ReceiveOnly);

            if (config.EnableInstallers)
                c.EnableInstallers();

            if (config.DisableAutoSubscribe)
                c.DisableFeature<AutoSubscribe>();

            if (config.SendOnly)
                c.SendOnly();

            c.Recoverability()
                .Immediate(retry => retry.NumberOfRetries(config.ImmediateNumberOfRetries))
                .Delayed(retry => retry.NumberOfRetries(config.DelayedNumberOfRetries)
                    .TimeIncrease(config.DelayedRetryTimeIncrease));

            config.Routing = routing;
            config.EndpointQueue = publicQueueName;

            return c;
        }

        private static EndpointConfiguration ExcludeAllAssembliesExceptStartingWith<TEndpoint>(this EndpointConfiguration c, params string[] prefixes)
        {
            var all = typeof(TEndpoint).Assembly
                .GetReferencedAssemblies()
                .Select(x => x.Name);

            var toExclude = all
                .Where(assembly => !prefixes.Any(prefix => assembly.StartsWith(prefix, StringComparison.OrdinalIgnoreCase)))
                .ToArray();

            c.AssemblyScanner().ExcludeAssemblies(toExclude);

            // If referenced message assembly is not called from code, it's not in the list
            // http://stackoverflow.com/questions/3433973/getreferencedassemblies-doesnt-return-all-assemblies

            //foreach (Type t in extraTypesToLoad)
            //    if (!assemblies.Contains(t.Assembly))
            //        assemblies.Add(t.Assembly);

            return c;
        }

        private static EndpointConfiguration ConfigurePersistence(this EndpointConfiguration c)
        {
            var connection = ConfigurationManager.ConnectionStrings["NServiceBus/Persistence"];
            if (connection == null)
                throw new ArgumentException("Connection string 'NServiceBus/Persistence' is required for NServiceBus SqlPersistence.");

            var persistence = c.UsePersistence<SqlPersistence>();
            persistence.SqlDialect<SqlDialect.MsSqlServer>();
            persistence.ConnectionBuilder(() => new SqlConnection(connection.ConnectionString));
            persistence.SubscriptionSettings().DisableCache();

            return c;
        }

        private static EndpointConfiguration ConfigureLogging(this EndpointConfiguration c)
        {
            ConfigureLog4Net.Programmatically(App.IsBuildConfigDebug);

            NServiceBus.Logging.LogManager.Use<Log4NetFactory>();

            return c;
        }

        private static EndpointConfiguration ConfigureMessageConventions<TEndpoint>(this EndpointConfiguration c)
        {
            c.Conventions()
                .DefiningCommandsAs(type => InMessageNamespace<TEndpoint>(type) && typeof(BaseCommand).IsAssignableFrom(type))
                .DefiningEventsAs(type => InMessageNamespace<TEndpoint>(type) && typeof(BaseEvent).IsAssignableFrom(type))
                .DefiningMessagesAs(type => InMessageNamespace<TEndpoint>(type) && typeof(BaseResponse).IsAssignableFrom(type));
            return c;
        }

        public static bool InMessageNamespace<TEndpoint>(Type x) => x.Namespace != null
            && (x.Namespace.Equals(typeof(TEndpoint).Namespace)
            || x.Namespace.StartsWith("Company.NSB.Messages", StringComparison.OrdinalIgnoreCase)
            || x.Namespace.StartsWith("Company.NSB.Common", StringComparison.OrdinalIgnoreCase));
        
        public static ILifetimeScope ConfigureContainer(this EndpointConfiguration c, Func<ContainerBuilder, ContainerBuilder> configureContainer)
        {
            var builder = new ContainerBuilder();

            var container = configureContainer(builder).Build();

            c.UseContainer<AutofacBuilder>(x => x.ExistingLifetimeScope(container));

            return container;
        }
    }

}