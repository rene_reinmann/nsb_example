﻿using System;

namespace Company.NSB.Common.Exceptions
{
    public class MessageRoutingException : Exception
    {
        public Type MessageType { get; }

        public MessageRoutingException(Type messageType) : this(messageType, $"Error routing message type '{messageType}'.")
        {
        }

        public MessageRoutingException(Type messageType, string message) : this(messageType, message, null)
        {

        }

        public MessageRoutingException(Type messageType, string message, Exception innerException) : base(message, innerException)
        {
            MessageType = messageType;
        }
    }
}