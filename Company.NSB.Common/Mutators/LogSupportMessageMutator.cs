﻿using System.Threading.Tasks;
using NServiceBus.MessageMutator;

namespace Company.NSB.Common.Mutators
{
    public class LogSupportMessageMutator : IMutateIncomingTransportMessages
    {
        public const string EndpointQueue = "EndpointQueue";

        private readonly string _queue;

        public LogSupportMessageMutator(string endpointQueue)
        {
            _queue = endpointQueue;
        }

        public Task MutateIncoming(MutateIncomingTransportMessageContext context)
        {
            context.Headers[EndpointQueue] = _queue;

            return Task.FromResult(0);
        }
    }

}