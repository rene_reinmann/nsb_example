﻿using System;
using System.Threading.Tasks;
using Company.NSB.Messages;
using NServiceBus.MessageMutator;

namespace Company.NSB.Common.Mutators
{
    public class ResponseMutator : IMutateOutgoingMessages
    {
        public Task MutateOutgoing(MutateOutgoingMessageContext context)
        {
            if (context.OutgoingMessage is BaseResponse response)
            {
                if (response.ToMessageId == default(Guid) &&
                    context.TryGetIncomingMessage(out var msg) && msg is BaseCommand command)
                    response.ToMessageId = command.MessageId;

                if (response.ResponseId == default(Guid))
                    response.ResponseId = new Guid();

                if (response.OccuredOn == default(DateTime))
                    response.OccuredOn = DateTime.Now;
            }

            return Task.FromResult(true);
        }
    }
}