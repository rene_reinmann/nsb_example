﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.NSB.Messages.Mutations;
using NServiceBus.MessageMutator;

namespace Company.NSB.Common.Mutators
{
    public class IncomingTypeMessageMutator : IMutateIncomingTransportMessages
    {
        private const string EnclosedMessageTypesHeader = NServiceBus.Headers.EnclosedMessageTypes;

        private readonly IDictionary<string, string> _incomingCache = new SortedDictionary<string, string>();

        private readonly IIncomingTypeMutatorProvider _incomingTypeMutatorProvider;
        private readonly IMutateIncomingType[] _incomingTypeMutators;

        public IncomingTypeMessageMutator(IIncomingTypeMutatorProvider incomingTypeMutatorProvider)
        {
            _incomingTypeMutatorProvider =
                incomingTypeMutatorProvider ?? throw new ArgumentNullException(nameof(incomingTypeMutatorProvider));

            if (_incomingTypeMutatorProvider == null)
                throw new ArgumentException(nameof(incomingTypeMutatorProvider));

            _incomingTypeMutators = _incomingTypeMutatorProvider.GetIncomingTypeMutators().ToArray();
        }

        public Task MutateIncoming(MutateIncomingTransportMessageContext context)
        {
            if (_incomingTypeMutators.Length == 0 || !context.Headers.ContainsKey(EnclosedMessageTypesHeader))
                return Task.CompletedTask;

            context.Headers[EnclosedMessageTypesHeader] =
                MutateIncomingEnclosedTypes(context.Headers[EnclosedMessageTypesHeader]);

            return Task.CompletedTask;
        }

        public string MutateIncomingEnclosedTypes(string enclosedMessageTypes)
        {
            if (_incomingCache.TryGetValue(enclosedMessageTypes, out string mutatedEnclosedMessageTypes))
                return mutatedEnclosedMessageTypes;

            IEnumerable<string> mutated = enclosedMessageTypes.Split(';')
                .Select(enclosedType => MutateMessageType(enclosedType.Trim()));

            mutatedEnclosedMessageTypes = string.Join("; ", mutated);

            _incomingCache[enclosedMessageTypes] = mutatedEnclosedMessageTypes;
            return mutatedEnclosedMessageTypes;
        }

        private string MutateMessageType(string messageType)
        {
            var typeChain = messageType.Split(new[] { ", " }, StringSplitOptions.None);

            const int fullNamePosition = 0;
            const int assemblyPosition = 1;

            var fullTypeName = typeChain[fullNamePosition];
            var assemblyName = typeChain[assemblyPosition];

            (string mutatedFullName, string mutatedAssemblyName) = MutateIncomingType(fullTypeName, assemblyName);

            typeChain[fullNamePosition] = mutatedFullName;
            typeChain[assemblyPosition] = mutatedAssemblyName;

            return string.Join(", ", typeChain);
        }

        private (string fullName, string assemblyName) MutateIncomingType(string fullName, string assemblyName)
        {
            foreach (var mutator in _incomingTypeMutators)
            {
                if (assemblyName.Equals(mutator.FromAssembly, StringComparison.OrdinalIgnoreCase))
                {
                    assemblyName = mutator.ToAssembly;
                    if (fullName.StartsWith(mutator.FromNamespace, StringComparison.OrdinalIgnoreCase))
                        fullName = mutator.ToNamespace + fullName.Substring(mutator.FromNamespace.Length);
                }
            }
            return (fullName, assemblyName);
        }
    }

}