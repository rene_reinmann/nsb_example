﻿using Autofac;
using NHibernate;
using NHibernate.Cfg;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using Company.DataModel;
using Company.Persitence.NHibernate;

namespace Company.NSB.Common
{
    public class RegisterNHibernateModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            ISessionFactory sessionFactory = Fluently
                .Configure(new Configuration())
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(x => x.FromConnectionStringWithKey("NHib"))
                    //.ShowSql()
                )
                .Mappings(mapping =>
                {
                    mapping.FluentMappings
                        .Conventions.Setup(x => x.Add(AutoImport.Never()))
                        .AddFromAssemblyOf<MapReference>();
                })
                .ExposeConfiguration(c => c.SetProperty("current_session_context_class", "thread_static"))
                //.ExposeConfiguration(c => c.SetProperty("default_flush_mode", "Commit"))
                .BuildConfiguration()
                //.SetInterceptor(new OriginInterceptor())
                .BuildSessionFactory();

            builder.RegisterGeneric(typeof(GenericDaoWithTypedId<,>)).As(typeof(IDaoWithTypedId<,>));
            builder.RegisterGeneric(typeof(GenericDao<>)).As(typeof(IDao<>));
            builder.RegisterInstance(sessionFactory).As<ISessionFactory>().SingleInstance();

            
            builder.Register(c =>
            {
                var session = c.Resolve<ISessionFactory>().OpenSession();
                session.FlushMode = FlushMode.Commit;
                return session;
            }).As<ISession>().InstancePerLifetimeScope();
        }
    }
}