﻿using System;
using System.Collections.Generic;
using System.Linq;
using Company.NSB.Messages;
using NServiceBus;

namespace Company.NSB.Common
{
    public abstract class NsbLogicalRouting
    {
        protected Dictionary<string, List<Type>> CommandMappings { get; set; }
        protected Dictionary<string, List<Type>> EventMappings { get; set; }

        protected Type ServiceEnumType { get; set; }

        protected NsbLogicalRouting()
        {
            CommandMappings = new Dictionary<string, List<Type>>();
            EventMappings = new Dictionary<string, List<Type>>();
        }

        public static List<string> Map<T>(RoutingSettings<MsmqTransport> routing) where T : NsbLogicalRouting, new()
        {
            var mapping = new T();

            if (mapping.ServiceEnumType != null)
                GetMappingFromAnnotationMessageRouter(mapping);

            var queues = new List<string>();

            foreach (KeyValuePair<string, List<Type>> e in mapping.CommandMappings)
            {
                foreach (Type m in e.Value)
                {
                    string queue = e.Key.ToLower();

                    routing.RouteToEndpoint(m, queue);

                    if (!queues.Contains(queue))
                        queues.Add(queue);
                }
            }

            foreach (KeyValuePair<string, List<Type>> e in mapping.EventMappings)
            {
                foreach (Type m in e.Value)
                {
                    string queue = e.Key.ToLower();

                    routing.RegisterPublisher(m, queue);

                    if (!queues.Contains(queue))
                        queues.Add(queue);
                }
            }

            return queues;
        }

        #region Annotation mapping router
        private static void GetMappingFromAnnotationMessageRouter(NsbLogicalRouting mapping)
        {
            var messageAssemblyProvider = new DefaultMessageAssembliesProvider();
            var messageTypesProvider = new DefaultMessageTypesProvider(messageAssemblyProvider);
            var messageRoutingProvider = new DefaultMessageRoutingProvider(messageAssemblyProvider);
            var messageRouter = new AnnotationMessageRouter(messageTypesProvider, messageRoutingProvider);

            (Type commandType, string endpoint)[] commandRouting = messageRouter.RouteAllCommands(mapping.ServiceEnumType);
            (Type eventType, string endpoint)[] eventRouting = messageRouter.RouteAllEvents(mapping.ServiceEnumType);

            foreach ((var commandType, var endpoint) in commandRouting)
            {
                var m = mapping.CommandMappings;

                if (!m.ContainsKey(endpoint))
                    m.Add(endpoint, new List<Type>());

                m[endpoint].Add(commandType);
            }

            foreach ((var eventType, var endpoint) in eventRouting)
            {
                var m = mapping.EventMappings;

                if (!m.ContainsKey(endpoint))
                    m.Add(endpoint, new List<Type>());

                m[endpoint].Add(eventType);
            }

            ThrowIfCommandRoutingConflictsDetected(mapping.CommandMappings);
        }
        #endregion

        #region Explicit mapping

        protected interface ITo
        {
            ITo Send<T>() where T : BaseCommand;
        }

        protected interface IFrom
        {
            IFrom Receive<T>() where T : BaseEvent;
        }

        protected ITo To(Enum endpoint)
        {
            return new CTo(this, GetQueue(endpoint));
        }

        protected IFrom From(Enum endpoint)
        {
            return new CFrom(this, GetQueue(endpoint));
        }

        private static string GetQueue(Enum endpoint)
        {
            var type = endpoint.GetType();
            var member = type.GetField(endpoint.ToString());
            var memberAttr = member.GetCustomAttributes(typeof(QueueAttribute), false).FirstOrDefault() as QueueAttribute;

            if (memberAttr != null)
                return memberAttr.Queue;

            var name = endpoint.ToString().ToLowerInvariant().Replace("_", ".");

            var typeAttr = type.GetCustomAttributes(typeof(QueueAttribute), false).FirstOrDefault() as QueueAttribute;

            return typeAttr != null ? $"{typeAttr.Queue}.{name}" : name;
        }

        protected class CTo : ITo
        {
            private readonly NsbLogicalRouting _config;
            private readonly string _queue;

            public CTo(NsbLogicalRouting config, string endpoint)
            {
                _config = config;
                _queue = endpoint;
            }

            public ITo Send<T>() where T : BaseCommand
            {
                _config.MapCommand(_queue, typeof(T));

                return this;
            }
        }

        protected class CFrom : IFrom
        {
            private readonly NsbLogicalRouting _config;
            private readonly string _queue;

            public CFrom(NsbLogicalRouting config, string endpoint)
            {
                _config = config;
                _queue = endpoint;
            }

            public IFrom Receive<T>() where T : BaseEvent
            {
                _config.MapEvent(_queue, typeof(T));

                return this;
            }
        }

        protected void MapCommand(string queue, Type message)
        {
            Map(queue, message, CommandMappings);
        }

        protected void MapEvent(string queue, Type message)
        {
            Map(queue, message, EventMappings);
        }

        private static void Map(string queue, Type message, IDictionary<string, List<Type>> mappings)
        {
            if (!mappings.ContainsKey(queue))
                mappings.Add(queue, new List<Type> { message });
            else
                mappings[queue].Add(message);
        }

        #endregion

        #region Conflict detection
        private static void ThrowIfCommandRoutingConflictsDetected(IDictionary<string, List<Type>> commandMappings)
        {
            Type[] allCommands = commandMappings.SelectMany(x => x.Value).ToArray();
            IGrouping<Type, Type>[] conflicts = allCommands.GroupBy(x => x)
                .Where(x => x.Count() > 1)
                .ToArray();

            if (conflicts.Any())
                throw new Exception(
                    $"The following commands have ambiguous routing:\n{string.Join("\n", conflicts.Select(x => x.Key))}");
        }
        #endregion
    }


}
