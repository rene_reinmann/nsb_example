﻿using log4net;
using Newtonsoft.Json;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.Routing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Company.Config;


namespace Company.NSB.Common
{
    #region feature

    public class NsbPhysicalRoutingFeature : Feature
    {
        public NsbPhysicalRoutingFeature()
        {
            EnableByDefault();
        }

        protected override void Setup(FeatureConfigurationContext context)
        {
            NsbPhysicalRouting.Get().ApplyRouting(context);
        }
    }

    #endregion

    public class NsbPhysicalRouting
    {
        public const string MAPPING_FILE = "serviceMap.json";

        public static readonly Dictionary<string, string> Machines = new Dictionary<string, string>
        {
            { "borrower", "AppMachineName" },
            { "investor", "CnodeMachineName" }
        };

        #region lazy singleton

        private static readonly Lazy<NsbPhysicalRouting> _this = new Lazy<NsbPhysicalRouting>(() => new NsbPhysicalRouting());

        public static NsbPhysicalRouting Get()
        {
            return _this.Value;
        }

        #endregion

        #region constructor

        private readonly ILog _logger = LogManager.GetLogger("NsbPhysicalRouting");
        private readonly List<EndpointInstance> _endpoints;
        private readonly List<string> _queues;

        private bool NoMapping => _queues == null;

        internal NsbPhysicalRouting(string mappingFile = MAPPING_FILE)
        {
            var mapping = ReadMapping(mappingFile);

            if (mapping == null)
                return;

            var queues = new List<string>();
            var endpoints = new List<EndpointInstance>();

            foreach (var machineKey in mapping.Keys)
            {
                string machineSettingName = $"{machineKey}MachineName";
                if (Machines.ContainsKey(machineKey))
                    machineSettingName = Machines[machineKey];

                string machine = System.Configuration.ConfigurationManager.AppSettings[machineSettingName];
                if (machine == null)
                    throw new Exception($"Missing configuration for key: [{machineSettingName}]");

                _logger.Info($"Mapping {machineKey} queues to machine [{machine}] ...");

                foreach (var queue in mapping[machineKey].Select(x => x.ToLower()))
                {
                    if (queues.Contains(queue))
                        throw new Exception($"NSB physical routing for the same queue cannot be specified multiple times: [{queue}]");

                    _logger.Info($"... [{queue}]");

                    if (machine != string.Empty)
                        endpoints.Add(new EndpointInstance(queue).AtMachine(machine));

                    queues.Add(queue);
                }

                _logger.Info("... done.");
            }

            _queues = queues;
            _endpoints = endpoints;
        }

        #endregion

        #region get physical address

        public string GetPhysicalAddress(string queue)
        {
            if (NoMapping || queue == "error2")
                return queue;

            var q = _queues.FirstOrDefault(x => string.Compare(x, queue, true) == 0);

            if (q == null)
                throw new Exception($"Cannot determine physical address for queue [{queue}]");

            var endpoint = _endpoints.FirstOrDefault(x => x.Endpoint == q);
            if (endpoint != null)
                queue = $"{endpoint.Endpoint}@{endpoint.Properties["machine"]}";

            return queue;
        }

        #endregion

        #region ensure routing exists

        public NsbPhysicalRouting EnsureRoutingExists(IEnumerable<string> queuesToCheck)
        {
            if (NoMapping)
                return this;

            _logger.Info("Checking physical mapping for the actually used queues ...");

            foreach (var queue in queuesToCheck)
            {
                _logger.Info($"... [{queue}]");

                if (!_queues.Contains(queue))
                {
                    _logger.Warn($"NSB physical routing information is missing for queue: [{queue}].");
                    var machine = System.Configuration.ConfigurationManager.AppSettings[Machines["borrower"]];
                    _endpoints.Add(new EndpointInstance(queue).AtMachine(machine));
                    _queues.Add(queue);
                }
            }

            _logger.Info("... done.");

            return this;
        }

        #endregion

        #region apply routing

        public NsbPhysicalRouting ApplyRouting(FeatureConfigurationContext context)
        {
            if (NoMapping)
                return this;

            var endpoints = context.Settings.Get<EndpointInstances>();

            endpoints.AddOrReplaceInstances("Deploy", _endpoints);

            return this;
        }

        #endregion

        #region read mapping from file

        private Dictionary<string, string[]> ReadMapping(string file)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file);

            if (!File.Exists(path))
            {
                //if (App.IsBuildConfigDebug)
                    return null;

                //throw new Exception($"NSB physical mapping file not found: [{path}]");
            }

            _logger.Info($"Reading service endpoints from [{file}]");

            return JsonConvert.DeserializeObject<Dictionary<string, string[]>>(File.ReadAllText(path));
        }

        #endregion
    }

}