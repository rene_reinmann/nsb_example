﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Company.NSB.Common.Exceptions;
using Company.NSB.Messages;

namespace Company.NSB.Common
{
    public sealed class AnnotationMessageRouter
    {
        private readonly IMessageTypesProvider _messageTypesProvider;
        private readonly IMessageRoutingProvider _messageRoutingProvider;

        public AnnotationMessageRouter(IMessageTypesProvider messageTypesProvider, IMessageRoutingProvider messageRoutingProvider)
        {
            _messageTypesProvider = messageTypesProvider
                                    ?? throw new ArgumentNullException(nameof(messageTypesProvider));
            _messageRoutingProvider = messageRoutingProvider
                                      ?? throw new ArgumentNullException(nameof(messageTypesProvider));
        }

        public (Type commandType, string endpoint)[] RouteAllCommands<TEnum>() where TEnum : Enum
        {
            return RouteAllCommands(typeof(TEnum));
        }

        public (Type commandType, string endpoint)[] RouteAllCommands(Type serviceEnumType)
        {
            if (!serviceEnumType.IsEnum)
                throw new ArgumentException($"Argument '{nameof(serviceEnumType)}' must be Enum type.");

            IDictionary<Enum, string> services = MapToEndpoints(serviceEnumType);

            IEnumerable<Type> commands = _messageTypesProvider.GetMessageTypes()
                .Where(x => typeof(BaseCommand).IsAssignableFrom(x) && !x.IsAbstract).ToList();

            var routing = commands
                .Where(x => x.GetCustomAttribute<HandledByAttribute>(false) != null)
                .Select(x => RouteCommand(services, x)).ToList();

            var notRoutedCommands = commands
                .Where(command => !routing.Any(x => x.command == command));

            var messageRouting = GetRoutingForMessageTypes(notRoutedCommands, services);
            if (messageRouting?.Count() > 0)
                routing.AddRange(messageRouting);

            return routing.ToArray();
        }

        public (Type eventType, string endpoint)[] RouteAllEvents<TEnum>() where TEnum : Enum
        {
            return RouteAllEvents(typeof(TEnum));
        }

        public (Type eventType, string endpoint)[] RouteAllEvents(Type serviceEnumType)
        {
            if (!serviceEnumType.IsEnum)
                throw new ArgumentException($"Argument '{nameof(serviceEnumType)}' must be Enum type.");

            IDictionary<Enum, string> services = MapToEndpoints(serviceEnumType);

            IEnumerable<Type> events = _messageTypesProvider.GetMessageTypes()
                .Where(x => typeof(BaseEvent).IsAssignableFrom(x) && !x.IsAbstract).ToList();

            var routing = events
                .Where(x => x.GetCustomAttribute<PublishedByAttribute>(false) != null)
                .Select(x => RouteEvent(services, x)).ToList();

            var notRoutedEvents = events
                .Where(eventType => !routing.Any(x => x.command == eventType));

            var messageRouting = GetRoutingForMessageTypes(notRoutedEvents, services);
            if (messageRouting?.Count() > 0)
                routing.AddRange(messageRouting);

            return routing.ToArray();
        }

        private IEnumerable<(Type eventType, string endpoint)> GetRoutingForMessageTypes(IEnumerable<Type> messageTypes, IDictionary<Enum, string> services)
        {
            var messageRoutings = _messageRoutingProvider
                .GetMessageRouting()
                .Where(x => messageTypes.Contains(x.MessageType));

            var typeRouting = new Dictionary<Type, Enum>();
            var typePriorities = new Dictionary<Type, int>();
            foreach (var messageRouting in messageRoutings)
            {
                var messageType = messageRouting.MessageType;
                if (typeRouting.ContainsKey(messageType) && typePriorities.TryGetValue(messageType, out int priority))
                {
                    if (priority > messageRouting.Priority)
                        continue;

                    if (priority == messageRouting.Priority)
                        throw new MessageRoutingException(messageType, $"Type '{messageType}' is already mapped to other service ({typeRouting[messageType]}) with same priority '{priority}'.");
                }
                typeRouting[messageType] = messageRouting.Endpoint;
                typePriorities[messageType] = messageRouting.Priority;
            }
            return typeRouting.Select(x => (x.Key, services[x.Value]));
        }

        private static (Type command, string endpoint) RouteCommand(IDictionary<Enum, string> services, Type commandType)
        {
            var mappedServiceAttribute = commandType.GetCustomAttribute<HandledByAttribute>(false);
            return (commandType, services[mappedServiceAttribute.Endpoint]);
        }

        private static (Type command, string endpoint) RouteEvent(IDictionary<Enum, string> services, Type eventType)
        {
            var mappedServiceAttribute = eventType.GetCustomAttribute<PublishedByAttribute>(false);
            return (eventType, services[mappedServiceAttribute.Endpoint]);
        }

        private static IDictionary<Enum, string> MapToEndpoints(Type services)
        {
            if (!services.IsEnum)
                throw new ArgumentException($"Argument '{nameof(services)}' must be Enum type.");

            var map = new SortedDictionary<Enum, string>();

            foreach (Enum service in Enum.GetValues(services))
            {
                MemberInfo[] member = services.GetMember(service.ToString());
                var attr = (EndpointAttribute)member[0].GetCustomAttributes(typeof(EndpointAttribute), false)[0];
                map[service] = attr.QueueName.Trim().ToLower();
            }

            return map;
        }
    }
}