﻿using System;
using System.Linq;
using NHibernate;
using NHibernate.SqlCommand;

namespace Company.NSB.Common
{
    /// <summary>
    /// Tags tags to NHibernate generated SQL queries to track the origination from the logs
    /// </summary>
    public class OriginInterceptor : EmptyInterceptor
    {
        private string _name;

        public string Name => _name ?? (_name = AppDomain.CurrentDomain.BaseDirectory.Split('\\').ToList()
                                  .FirstOrDefault(x => x.Contains("Company."))?
                                  .Replace(".NSB.Services.", string.Empty));

        public override SqlString OnPrepareStatement(SqlString sql)
        {
            sql = sql.Insert(0, $"/*__FROM:{Name}__*/");
            return base.OnPrepareStatement(sql);
        }
    }
}