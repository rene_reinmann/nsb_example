﻿using System;
using System.Collections.Generic;

namespace Company.NSB.Common
{
    public interface IEndpointConfig
    {
        string EndpointName { get; set; }
        string EndpointQueue { get; set; }
        string AuditQueue { get; set; }
        string ErrorQueue { get; set; }
        bool EnableInstallers { get; set; }
        bool MultithreadingEnabled { get; set; }
        bool DisableDTC { get; set; }
        bool DisableAutoSubscribe { get; set; }
        int MessageProcessingConcurrency { get; set; }
        int ImmediateNumberOfRetries { get; set; }
        int DelayedNumberOfRetries { get; set; }
        TimeSpan DelayedRetryTimeIncrease { get; set; }
        List<string> Routing { get; set; }
        bool UseJsonSerializer { get; set; }
        bool CallbacksMakeRequests { get; set; }
        bool SendOnly { get; set; }
    }
}