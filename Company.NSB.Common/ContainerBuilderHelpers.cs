﻿using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;

namespace Company.NSB.Common
{
    public static class ContainerBuilderHelpers
    {
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> RegisterDaoTypes(
            this ContainerBuilder builder)
        {
            return builder.RegisterAssemblyTypes("Company.Dao", "Dao");
        }

        private static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle>
            RegisterAssemblyTypes(
                this ContainerBuilder builder, string assemblyName, string endsWithFilter = null,
                bool propertiesAutoWired = true, bool asImplementedInterfaces = true)
        {
            var assembly = GetAssembly(assemblyName);
            var registration = builder.RegisterAssemblyTypes(assembly);

            if (!string.IsNullOrEmpty(endsWithFilter))
                registration = registration.Where(t => t.Name.EndsWith(endsWithFilter));
            if (propertiesAutoWired)
                registration = registration.PropertiesAutowired(PropertyWiringOptions.PreserveSetValues);
            if (asImplementedInterfaces)
                registration = registration.AsImplementedInterfaces();

            return registration;
        }

        private static Assembly GetAssembly(string assemblyName)
        {
            string assemblyFile = GetAssemblyFilePath(assemblyName);
            return Assembly.LoadFrom(assemblyFile);
        }

        private static string GetAssemblyFilePath(string assemblyName)
        {
            return System.IO.Path.Combine(ApplicationPath, $"{assemblyName}.dll");
        }

        public static string ApplicationPath => string.IsNullOrEmpty(System.AppDomain.CurrentDomain.RelativeSearchPath)
            ? System.AppDomain.CurrentDomain.BaseDirectory
            : System.AppDomain.CurrentDomain.RelativeSearchPath;
    }
}