﻿using System.Linq;

namespace Company.Persitence.NHibernate
{
    // <summary>


    /// <summary>
    ///     Since nearly all of the domain objects you create will have a type of int ID, this
    ///     most freqently used base IDao leverages this assumption.
    /// </summary>
    public interface IDao<T> : IDaoWithTypedId<T, int>
    {
    }

    public interface IDaoWithTypedId<T, in TId>
    {
        /// <summary>
        ///     Technically, this isn't DAO specific and flushes everything that has been changed since
        ///     the last commit; but it's convenient putting it here so I'm not going to sweat it too much.
        /// </summary>
        void CommitChanges();
        void CommitChanges(int batchSize);
        void CommitAndClearChanges();

        void Delete(T entity);

        /// <summary>
        ///     Dissasociates the entity with the ORM so that changes made to it are not automatically
        ///     saved to the database.  More precisely, this removes the entity from ISession's cache.
        ///     More details may be found at http://www.hibernate.org/hib_docs/nhibernate/html_single/#performance-sessioncache.
        /// </summary>
        void Evict(T entity);

        /// <summary>
        /// Evict all loaded instances and cancel all pending saves, updates and deletions.
        /// </summary>
        void Clear();

        /// <summary>
        ///     Throws an exception if a row is not found matching the provided ID.
        /// </summary>
        T Load(TId id);
        T Load(TId id, LockMode lockMode);
        IQueryable<T> LoadAll();

        /// <summary>
        ///     For entities that have assigned ID's, you must explicitly call Save to add a new one.
        ///     See http://www.hibernate.org/hib_docs/nhibernate/html_single/#mapping-declaration-id-assigned.
        /// </summary>
        T Save(T entity);

        /// <summary>
        ///     For entities with automatatically generated IDs, such as identity, SaveOrUpdate may
        ///     be called when saving or updating an entity.  Although SaveOrUpdate _can_ be invoked
        ///     to update an object with an assigned ID, you are hereby forced instead to use Save/Update
        ///     for better clarity.
        ///     Updating also allows you to commit changes to a detached object.  More info may be found at:
        ///     http://www.hibernate.org/hib_docs/nhibernate/html_single/#manipulatingdata-updating-detached
        /// </summary>
        T SaveOrUpdate(T entity);

        /// <summary>
        ///     For entities that have assigned ID's, you should explicitly call Update to update an existing one.
        ///     Updating also allows you to commit changes to a detached object.  More info may be found at:
        ///     http://www.hibernate.org/hib_docs/nhibernate/html_single/#manipulatingdata-updating-detached
        /// </summary>
        T Update(T entity);

        /// <summary>
        /// Explicitly set entity to mutable (ReadOnly = false).
        /// If entity is set to read-only by session, set to mutable (ReadOnly = false).
        /// This is need if you want to change the entity and persist.
        /// </summary>
        T LoadAsMutable(TId id);

        /// <summary>
        /// Explicitly set loaded entity as Read-Only (does not track modifications and persist changes on flush).
        /// </summary>
        T LoadAsReadOnly(TId id);

        /// <summary>
        /// Change a read-only entity or proxy of a mutable class so it is no longer read-only.
        /// http://nhibernate.info/doc/nhibernate-reference/readonly.html#readonly-api-entity
        /// </summary>
        void ChangeToMutable(T entity);

        /// <summary>
        /// Make a persistent entity or proxy read-only.
        /// http://nhibernate.info/doc/nhibernate-reference/readonly.html#readonly-api-entity
        /// </summary>
        void ChangeToReadOnly(T entity);

        /// <summary>
        /// Throw away non-flushed changes and make the persistent entity consistent with its database representation.
        /// http://nhibernate.info/doc/nhibernate-reference/readonly.html#readonly-api-entity
        /// </summary>
        void Refresh(T entity);
    }

}