﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;

namespace Company.Persitence.NHibernate
{
    /// <summary>
    ///     Since nearly all of the domain objects you create will have a type of int ID, this
    ///     most freqently used base GenericDao leverages this assumption.
    /// </summary>
    public class GenericDao<T> : GenericDaoWithTypedId<T, int>, IDao<T>
    {
        public GenericDao(ISession session)
            : base(session)
        {
        }
    }

    /// <summary>
    ///     Provides a fully loaded DAO which may be created in a few ways
    /// </summary>
    public class GenericDaoWithTypedId<T, TId> : IDaoWithTypedId<T, TId>
    {
        public GenericDaoWithTypedId()
        {
        }

        public GenericDaoWithTypedId(ISession session)
        {
            if (session == null)
                throw new ArgumentNullException("session");

            Session = session;
        }

        public ISession Session { get; set; }

        /// <summary>
        ///     Technically, this isn't DAO specific and flushes everything that has been changed since
        ///     the last commit; but it's convenient putting it here so I'm not going to sweat it too much.
        /// </summary>
        public void CommitChanges()
        {
            Session.Flush();
        }

        public void CommitChanges(int batchSize)
        {
            Session.SetBatchSize(batchSize);
            Session.Flush();
        }

        public void CommitAndClearChanges()
        {
            Session.Flush();
            Session.Clear();
        }

        public void Delete(T entity)
        {
            Session.Delete(entity);
        }

        /// <summary>
        ///     Dissasociates the entity with the ORM so that changes made to it are not automatically
        ///     saved to the database.  More precisely, this removes the entity from <see cref="ISession" />'s cache.
        ///     More details may be found at http://www.hibernate.org/hib_docs/nhibernate/html_single/#performance-sessioncache.
        /// </summary>
        public void Evict(T entity)
        {
            Session.Evict(entity);
        }

        /// <summary>
        /// Evict all loaded instances and cancel all pending saves, updates and deletions.
        /// </summary>
        public void Clear()
        {
            Session.Clear();
        }

        /// <summary>
        ///     Throws an exception if a row is not found matching the provided ID.
        /// </summary>
        public virtual T Load(TId id)
        {
            return Load(id, LockMode.None);
        }

        /// <summary>
        ///     Throws an exception if a row is not found matching the provided ID.
        /// </summary>
        public T Load(TId id, LockMode lockMode)
        {
            return Session.Get<T>(id, NHibernateLockTypeConverter.ConvertFrom(lockMode));
        }

        public IQueryable<T> LoadAll()
        {
            return Session.Query<T>();
        }

        /// <summary>
        /// Explicitly set entity to mutable (ReadOnly = false).
        /// If entity is set to read-only by session, set to mutable (ReadOnly = false).
        /// This is need if you want to change the entity and persist.
        /// </summary>
        public T LoadAsMutable(TId id)
        {
            T entity = Load(id);
            if (entity != null)
            {
                if (Session.IsReadOnly(entity))
                    Session.SetReadOnly(entity, false);
            }
            return entity;
        }

        /// <summary>
        /// Explicitly set loaded entity as Read-Only (does not track modifications and persist changes on flush).
        /// </summary>
        public T LoadAsReadOnly(TId id)
        {
            T entity = Load(id);
            if (entity != null)
            {
                Session.SetReadOnly(entity, true);
            }
            return entity;
        }

        /// <summary>
        ///     For entities that have assigned ID's, you must explicitly call Save to add a new one.
        ///     See http://www.hibernate.org/hib_docs/nhibernate/html_single/#mapping-declaration-id-assigned.
        /// </summary>
        public T Save(T entity)
        {
            Session.Save(entity);

            return entity;
        }

        /// <summary>
        ///     For entities with automatatically generated IDs, such as identity, SaveOrUpdate may
        ///     be called when saving or updating an entity.  Although SaveOrUpdate _can_ be invoked
        ///     to update an object with an assigned ID, you are hereby forced instead to use Save/Update
        ///     for better clarity.
        ///     Updating also allows you to commit changes to a detached object.  More info may be found at:
        ///     http://www.hibernate.org/hib_docs/nhibernate/html_single/#manipulatingdata-updating-detached
        /// </summary>
        public T SaveOrUpdate(T entity)
        {
            if (entity is IHasAssignedId<TId>)
                throw new InvalidOperationException(
                    "For better clarity and reliability, PersistentObjects with an assigned ID must call Save or Update");

            Session.SaveOrUpdate(entity);

            return entity;
        }

        /// <summary>
        ///     For entities that have assigned ID's, you should explicitly call Update to update an existing one.
        ///     Updating also allows you to commit changes to a detached object.  More info may be found at:
        ///     http://www.hibernate.org/hib_docs/nhibernate/html_single/#manipulatingdata-updating-detached
        /// </summary>
        public T Update(T entity)
        {
            Session.Update(entity);

            return entity;
        }

        /// <summary>
        /// Change a read-only entity or proxy of a mutable class so it is no longer read-only.
        /// http://nhibernate.info/doc/nhibernate-reference/readonly.html#readonly-api-entity
        /// </summary>
        public void ChangeToMutable(T entity)
        {
            Session.SetReadOnly(entity, false);
        }

        /// <summary>
        /// Make a persistent entity or proxy read-only.
        /// http://nhibernate.info/doc/nhibernate-reference/readonly.html#readonly-api-entity
        /// </summary>
        public void ChangeToReadOnly(T entity)
        {
            Session.SetReadOnly(entity, true);
        }

        /// <summary>
        /// Throw away non-flushed changes and make the persistent entity consistent with its database representation.
        /// http://nhibernate.info/doc/nhibernate-reference/readonly.html#readonly-api-entity
        /// </summary>
        public void Refresh(T entity)
        {
            Session.Refresh(entity);
        }
    }

}
