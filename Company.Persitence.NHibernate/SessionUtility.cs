﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Metadata;

namespace Company.Persitence.NHibernate
{
    public class SessionUtility : ISessionUtility
    {
        private readonly ISession _session;

        public SessionUtility(ISession session)
        {
            if (session == null)
                throw new ArgumentNullException();

            _session = session;
        }

        public void Flush()
        {
            _session.Flush();
        }

        /// <summary>
        ///     Clears NHibernate first level cache and, optionally, second level cache.
        /// </summary>
        /// <param name="fullWipe">Set to true to also clear second level cache.</param>
        public void Clear(bool fullWipe = false)
        {
            _session.Clear();

            if (!fullWipe)
                return;

            ISessionFactory sessionFactory = _session.SessionFactory;

            sessionFactory.EvictQueries();

            foreach (
                KeyValuePair<string, ICollectionMetadata> collectionMetadata in
                sessionFactory.GetAllCollectionMetadata())
                sessionFactory.EvictCollection(collectionMetadata.Key);

            foreach (KeyValuePair<string, IClassMetadata> classMetadata in sessionFactory.GetAllClassMetadata())
                sessionFactory.EvictEntity(classMetadata.Key);
        }
    }
}