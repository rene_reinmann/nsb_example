﻿using System;

namespace Company.Persitence.NHibernate
{
    /// <summary>
    ///     For a discussion of this object, see
    ///     http://devlicio.us/blogs/billy_mccafferty/archive/2007/04/25/using-equals-gethashcode-effectively.aspx
    /// </summary>
    public abstract class PersistentObjectWithTypedId<TId>
    {
        #region Properties

        /// <summary>
        ///     ID may be of type string, int, custom type, etc.
        ///     Setter is protected to allow unit tests to set this property via reflection and to allow
        ///     domain objects more flexibility in setting this for those objects with assigned IDs.
        ///     It's virtual to allow NHibernate-backed objects to be lazily loaded.
        /// </summary>
        public virtual TId ID { get; protected set; }

        #endregion Properties

        #region Fields

        #endregion Fields

        #region Methods

        public override bool Equals(object obj)
        {
            return obj is PersistentObjectWithTypedId<TId> compareTo &&
                   (HasSameNonDefaultIdAs(compareTo) ||
                    // Since the IDs aren't the same, either of them must be transient to
                    // compare business value signatures
                    (IsTransient() || compareTo.IsTransient()) &&
                    HasSameBusinessSignatureAs(compareTo));
        }

        /// <summary>
        ///     Must be provided to properly compare two objects.  This is sealed because the hash code
        ///     is created using the result of <see cref="GetDomainObjectSignature" />.
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    GetDomainObjectSignature()).GetHashCode();
        }

        /// <summary>
        ///     Transient objects are not associated with an item already in storage.  For instance,
        ///     a Customer is transient if its ID is 0.  It's virtual to allow NHibernate-backed
        ///     objects to be lazily loaded.
        /// </summary>
        public virtual bool IsTransient()
        {
            return ID == null || ID.Equals(default(TId));
        }

        /// <summary>
        ///     The overriding method GetDomainObjectSignature should ONLY contain the "business value
        ///     signature" of the object and not the ID. The general structure of the overridden method should be as follows:
        ///     return SomeProperty + "|" + SomeOtherProperty;
        /// </summary>
        protected abstract string GetDomainObjectSignature();

        private bool HasSameBusinessSignatureAs(PersistentObjectWithTypedId<TId> other)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            return GetHashCode().Equals(other.GetHashCode());
        }

        /// <summary>
        ///     Returns true if self and the provided persistent object have the same ID values
        ///     and the IDs are not of the default ID value
        /// </summary>
        private bool HasSameNonDefaultIdAs(PersistentObjectWithTypedId<TId> other)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            return ID != null && !ID.Equals(default(TId)) && other.ID != null && !other.ID.Equals(default(TId)) &&
                   ID.Equals(other.ID);
        }

        #endregion Methods
    }
}