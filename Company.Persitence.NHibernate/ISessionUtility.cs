﻿namespace Company.Persitence.NHibernate
{
    public interface ISessionUtility
    {
        /// <summary>
        ///     Clears NHibernate first level cache and, optionally, second level cache.
        /// </summary>
        /// <param name="fullWipe">Set to true to also clear second level cache.</param>
        void Clear(bool fullWipe = false);

        void Flush();
    }
}