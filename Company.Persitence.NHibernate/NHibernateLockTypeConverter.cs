﻿using System;
using System.Reflection;

namespace Company.Persitence.NHibernate
{
    public class NHibernateLockTypeConverter
    {
        /// <summary>
        ///     Translates a domain layer lock mode into an NHibernate lock mode via reflection.  This is
        ///     provided to facilitate developing the domain layer without a direct dependency on the
        ///     NHibernate assembly.
        /// </summary>
        public static global::NHibernate.LockMode ConvertFrom(LockMode lockMode)
        {
            FieldInfo translatedLockMode = typeof(global::NHibernate.LockMode).GetField(lockMode.ToString(),
                BindingFlags.Public | BindingFlags.Static);

            if (translatedLockMode == null)
                throw new InvalidOperationException("The provided lock mode , '" + lockMode + ",' " +
                                                    "could not be translated into an NHibernate.LockMode. This is probably because " +
                                                    "NHibernate was updated and now has different lock modes which are out of synch " +
                                                    "with the lock modes maintained in the domain layer.");

            return (global::NHibernate.LockMode)translatedLockMode.GetValue(null);
        }
    }
}