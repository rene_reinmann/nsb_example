﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace Company.Common.Logging
{
    public class ConfigureLog4Net
    {
        private static readonly object Lock = new object();

        private static string _baseDir;

        public static bool IsWeb => _baseDir.Contains(".web.") || _baseDir.Contains(".api.") || _baseDir.EndsWith(".web") || _baseDir.EndsWith(".api");

        public static void Programmatically(string baseDir, bool isDebug, Dictionary<string, string> customLogLevels = null)
        {
            lock (Lock)
                _baseDir = baseDir.ToLower();
            Programmatically(isDebug, customLogLevels);
        }

        /// <summary>
        /// Configures Log4net appenders, default level depends on <param name="isDebug" />
        /// </summary>
        /// <param name="isDebug">Sets default level to info and adds a ConsoleAppender</param>
        /// <param name="customLogLevels">Logger name and loglevel (Debug, Info, Warn, Error)</param>
        public static void Programmatically(bool isDebug, Dictionary<string, string> customLogLevels = null)
        {
            lock (Lock)
                ProgrammaticallyLocked(isDebug, customLogLevels);
        }

        private static void ProgrammaticallyLocked(bool isDebug, Dictionary<string, string> customLogLevels = null)
        {
            if (string.IsNullOrEmpty(_baseDir))
                _baseDir = AppDomain.CurrentDomain.BaseDirectory.ToLower();

            var hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.ResetConfiguration();

            var patternLayout = new PatternLayout { ConversionPattern = "%date [%thread] %-5level %logger %message%newline" };
            patternLayout.ActivateOptions();

            string level = ConfigurationManager.AppSettings["LogLevel"];


            hierarchy.Root.AddAppender(GetRollingFileAppender(patternLayout));

            if (isDebug)
            {
                // Default to info level and add consoleappender
                hierarchy.Root.AddAppender(GetColoredConsoleAppender(patternLayout));
                if (string.IsNullOrEmpty(level))
                    level = "info";
            }

            if (string.IsNullOrEmpty(level))
                level = "error";

            switch (level.ToLower())
            {
                case "debug":
                    hierarchy.Root.Level = Level.Debug;
                    break;
                case "info":
                    hierarchy.Root.Level = Level.Info;
                    break;
                case "warn":
                    hierarchy.Root.Level = Level.Warn;
                    break;
                case "error":
                    hierarchy.Root.Level = Level.Error;
                    break;
            }

            if (customLogLevels != null)
            {
                foreach (string key in customLogLevels.Keys)
                {
                    var logger = (Logger)hierarchy.GetLogger(key);
                    logger.Level = logger.Hierarchy.LevelMap[customLogLevels[key].ToUpper()];
                }
            }

            //if (ConfigurationManager.AppSettings["DebugNHib"] == null)
            //{
            //    // Too chatty when on info level, turn this on via conf explicitly
            //    var nHibernate = (Logger)hierarchy.GetLogger("NHibernate");
            //    nHibernate.Level = nHibernate.Hierarchy.LevelMap["WARN"];
            //}

            hierarchy.Configured = true;
        }

        /// <summary>
        ///     Tries to find service name from AppDomain directory
        /// </summary>
        private static string GetServiceName()
        {
            string serviceName = null;
            List<string> dirChunks = _baseDir.Split(new[] { '\\' }, StringSplitOptions.RemoveEmptyEntries).Reverse().ToList();

            // Get project name from the path, e.g. C:\BusinessServices\Sobralaen.NSB.Services.EventLog > Sobralaen.NSB.Services.EventLog
            foreach (string chunk in dirChunks)
            {
                string[] excluded = { "bin", "debug", "release" };

                if (serviceName != null)
                    break;

                if (excluded.Contains(chunk))
                    continue;

                serviceName = chunk;
            }

            if (string.IsNullOrEmpty(serviceName) || serviceName.Length < 4)
                throw new Exception($"Invalid service name: '{serviceName}'");

            // Transform project name to service name e.g. Sobralaen.NSB.Services.EventLog > SobralaenEventLog
            return serviceName.Replace(".nsb.services.", "");
        }

        private static RollingFileAppender GetRollingFileAppender(PatternLayout patternLayout)
        {
            string logBase = @"c:\temp\logs\";

            if (IsWeb)
                logBase = Path.Combine(logBase, "web");

            var roller = new RollingFileAppender
            {
                AppendToFile = true,
                File = Path.Combine(logBase, $"{GetServiceName()}.log"),
                Layout = patternLayout,
                MaxSizeRollBackups = 10,
                MaximumFileSize = "200KB",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            roller.ActivateOptions();
            return roller;
        }

        private static ColoredConsoleAppender GetColoredConsoleAppender(PatternLayout patternLayout)
        {
            var console = new ColoredConsoleAppender
            {
                Layout = patternLayout
            };
            console.AddMapping(new ColoredConsoleAppender.LevelColors
            {
                Level = Level.Warn,
                ForeColor = ColoredConsoleAppender.Colors.Yellow
            });
            console.AddMapping(new ColoredConsoleAppender.LevelColors
            {
                Level = Level.Error,
                ForeColor = ColoredConsoleAppender.Colors.Red
            });
            console.AddMapping(new ColoredConsoleAppender.LevelColors
            {
                Level = Level.Fatal,
                ForeColor = ColoredConsoleAppender.Colors.White,
                BackColor = ColoredConsoleAppender.Colors.Red
            });
            console.AddMapping(new ColoredConsoleAppender.LevelColors
            {
                Level = Level.Info,
                ForeColor = ColoredConsoleAppender.Colors.Cyan
            });
            console.AddMapping(new ColoredConsoleAppender.LevelColors
            {
                Level = Level.Debug,
                ForeColor = ColoredConsoleAppender.Colors.Green
            });
            console.ActivateOptions();
            return console;
        }
    }

}
