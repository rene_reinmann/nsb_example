﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Company.Data.Migrations
{
    [Migration(201911181630)]
    public class _201911181630_AddTestTables : Migration
    {
        public override void Up()
        {
            //Execute.Sql("CREATE DATABASE [CompanyDB]");

            Execute.Sql(@"
                USE CompanyDB;
                GO


                CREATE TABLE dbo.[TestObjectRequest](
	                [TestObjectId] [uniqueidentifier] NOT NULL,
	                [ApplicationStartedDate] [datetime] NOT NULL,
	                [ApplicationSubmittedDate] [datetime] NOT NULL,
	                [Number] [int] NOT NULL,
	                [AppliedAmount] [decimal](18, 2) NOT NULL,
	                [Interest] [decimal](18, 2) NOT NULL,
	                [Country] [nvarchar](2) NOT NULL,
	                [Duration] [smallint] NOT NULL,
	                [MonthlyPaymentDay] [smallint] NOT NULL,
	                [MonthlyPayment] [decimal](18, 2) NOT NULL,
	                [MonthlyLivingExpenses] [decimal](18, 2) NOT NULL,
	                [DebtToIncome] [decimal](18, 2) NOT NULL,
	                [FreeCash] [decimal](18, 2) NOT NULL,
	                [AnnualPercentageRate ] [decimal](18, 2) NULL,
	                [PartyId] [uniqueidentifier] NOT NULL,
	                [Username] [nvarchar](50) NOT NULL,
	                [ReferralNr] [nvarchar](25) NOT NULL,
	                [AddressCountry] [nvarchar](100) NOT NULL,
	                [AddressCounty] [nvarchar](100) NOT NULL,
	                [AddressCity] [nvarchar](100) NOT NULL,
	                [DateOfBirth] [date] NULL,
	                [Education] [smallint] NOT NULL,
	                [EmploymentDurationCurrentEmployer] [nvarchar](100) NULL,
	                [EmploymentPosition] [nvarchar](100) NULL,
	                [EmploymentStatus] [smallint] NOT NULL,
	                [Gender] [smallint] NOT NULL,
	                [HomeOwnershipType] [smallint] NOT NULL,
	                [LanguageCode] [smallint] NOT NULL,
	                [MaritalStatus] [smallint] NOT NULL,
	                [NrOfDependants] [nvarchar](100) NULL,
	                [OccupationArea] [smallint] NOT NULL,
	                [WorkExperience] [nvarchar](100) NULL,
	                [IncomeAndExpensesVerificationType] [smallint] NOT NULL,
	                [IncomeFromPrincipalEmployer] [decimal](18, 2) NOT NULL,
	                [IncomeFromPension] [decimal](18, 2) NOT NULL,
	                [IncomeFromFamilyAllowance] [decimal](18, 2) NOT NULL,
	                [IncomeFromSocialWelfare] [decimal](18, 2) NOT NULL,
	                [IncomeFromLeavePay] [decimal](18, 2) NOT NULL,
	                [IncomeFromChildSupport] [decimal](18, 2) NOT NULL,
	                [IncomeOther] [decimal](18, 2) NOT NULL,
	                [IncomeTotal] [decimal](18, 2) NOT NULL,
	                [NewCreditCustomer] [bit] NOT NULL,
	                [CreditScore] [nvarchar](10) NULL,
	                [Rating] [nvarchar](3) NULL,
	                [ModelVersion] [nvarchar](100) NULL,
	                [ExpectedLoss] [float] NOT NULL,
	                [ExpectedReturn] [float] NOT NULL,
	                [LossGivenDefault] [float] NOT NULL,
	                [ExposureAtDefault] [float] NOT NULL,
	                [ProbabilityOfDefault] [float] NOT NULL,
	                [ProbabilityOfBad] [float] NOT NULL,
                 CONSTRAINT [PK_TestObjectRequest] PRIMARY KEY CLUSTERED 
                (
	                [TestObjectId] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY]
                GO



                CREATE TABLE dbo.[TestObjectRequestDebt](
	                [Id] [uniqueidentifier] NOT NULL,
	                [TestObjectId] [uniqueidentifier] NOT NULL,
	                [StartDate] [date] NULL,
	                [EndDate] [date] NULL,
	                [Amount] [nvarchar](100) NULL,
	                [MaxAmount] [nvarchar](100) NULL,
	                [Industry] [nvarchar](100) NULL,
	                [Reporter] [nvarchar](100) NULL,
                 CONSTRAINT [PK_TestObjectRequestDebt] PRIMARY KEY CLUSTERED 
                (
	                [Id] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY]
                GO

                ALTER TABLE dbo.TestObjectRequestDebt  WITH NOCHECK ADD  CONSTRAINT [FK_TestObjectRequestDebt_TestObjectRequest] FOREIGN KEY(testobjectid)
                REFERENCES dbo.TestObjectRequest (testobjectid)
                GO

                ALTER TABLE dbo.TestObjectRequestDebt NOCHECK CONSTRAINT [FK_TestObjectRequestDebt_TestObjectRequest]
                GO



                CREATE TABLE dbo.[TestObjectRequestLiability](
	                [Id] [uniqueidentifier] NOT NULL,
	                [TestObjectId] [uniqueidentifier] NOT NULL,
	                [CreditorName] [nvarchar](250) NOT NULL,
	                [MonthlyPaymentAmount] [decimal](18, 2) NOT NULL,
	                [OutstandingAmount] [decimal](18, 2) NULL,
	                [CollateralType] [smallint] NOT NULL,
	                [LiabilityType] [smallint] NOT NULL,
	                [WillBeRefinanced] [bit] NOT NULL,
                 CONSTRAINT [PKTestObjectRequestLiability] PRIMARY KEY CLUSTERED 
                (
	                [Id] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY]
                GO

                ALTER TABLE dbo.TestObjectRequestLiability  WITH NOCHECK ADD  CONSTRAINT [FK_TestObjectRequestLiability_TestObjectRequest] FOREIGN KEY([TestObjectId])
                REFERENCES dbo.[TestObjectRequest] ([TestObjectId])
                GO

                ALTER TABLE dbo.TestObjectRequestLiability NOCHECK CONSTRAINT [FK_TestObjectRequestLiability_TestObjectRequest]
                GO

                                
                CREATE TABLE [dbo].[SomeTable](
	                [TestObjectId] [uniqueidentifier] NOT NULL,
	                [Number] [int] NOT NULL,
	                [PartyId] [uniqueidentifier] NULL,
	                [UserName] [nvarchar](256) NOT NULL,
	                [NewCreditCustomer] [bit] NOT NULL,
	                [ApplicationStartedDate] [datetime] NOT NULL,
	                [ApplicationSignedHour] [int] NULL,
	                [ApplicationSignedWeekday] [int] NULL,
	                [VerificationType] [smallint] NULL,
	                [LanguageCode] [smallint] NOT NULL,
	                [Age] [int] NOT NULL,
	                [Gender] [int] NULL,
	                [Country] [nvarchar](2) NOT NULL,
	                [AppliedAmount] [decimal](18, 4) NOT NULL,
	                [Interest] [decimal](18, 2) NOT NULL,
	                [Duration] [bigint] NOT NULL,
	                [County] [nvarchar](100) NOT NULL,
	                [City] [nvarchar](100) NOT NULL,
	                [Education] [smallint] NULL,
	                [MaritalStatus] [smallint] NULL,
	                [NrOfDependants] [nvarchar](200) NULL,
	                [EmploymentStatus] [smallint] NULL,
	                [EmploymentDurationCurrentEmployer] [nvarchar](200) NULL,	
	                [EmploymentPosition] [nvarchar](200) NULL,
	                [WorkExperience] [nvarchar](200) NULL,
	                [OccupationArea] [smallint] NULL,
	                [HomeOwnershipType] [smallint] NULL,
	                [IncomeFromPrincipalEmployer] [decimal](18, 2) NOT NULL,
	                [IncomeFromPension] [decimal](18, 2) NOT NULL,
	                [IncomeFromFamilyAllowance] [decimal](18, 2) NOT NULL,
	                [IncomeFromSocialWelfare] [decimal](18, 2) NOT NULL,
	                [IncomeFromLeavePay] [decimal](18, 2) NOT NULL,
	                [IncomeFromChildSupport] [decimal](18, 2) NOT NULL,
	                [IncomeOther] [decimal](18, 2) NOT NULL,
	                [IncomeTotal] [decimal](18, 2) NOT NULL,
	                [MonthlyPaymentDay] [int] NOT NULL,
	                [ModelVersion] [int] NULL,
	                [ExpectedLoss] [float] NULL,
	                [Rating] [nvarchar](3) NULL,
	                [LossGivenDefault] [float] NULL,
	                [ProbabilityOfDefault] [float] NULL,
	                [ExpectedReturnAlpha] [float] NULL,
	                [ListedOnUTC] [datetime] NOT NULL,
	                [LiabilitiesTotal] [decimal](18, 5) NOT NULL,
	                [ExistingLiabilities] [smallint] NULL,
	                [RefinanceLiabilities] [smallint] NULL,
	                [MonthlyPayment] [decimal](18, 4) NULL,
	                [FreeCash] [decimal](18, 4) NULL,
	                [CreditScore] [nvarchar](10) NULL,
	                [DateOfBirth] [date] NULL,
	                [EADRate] [float] NULL,
                 CONSTRAINT [PK_SomeTable] PRIMARY KEY CLUSTERED 
                (
	                [TestObjectId] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ) ON [PRIMARY]
                GO


                ALTER TABLE dbo.[SomeTable] ADD  CONSTRAINT [DF_SomeTable_ListedOnUTC]  DEFAULT (getutcdate()) FOR [ListedOnUTC]
                GO


                CREATE TABLE [dbo].[TestObject](
	                [ID] [uniqueidentifier] NOT NULL,
                    [Version] int NOT NULL,
                    [CreatedOn] [datetime] NOT NULL,
					[Country] NVARCHAR(2) NOT NULL,
					[PartyId] UNIQUEIDENTIFIER NOT NULL,					
					[Amount] decimal(18, 2) NOT NULL,
					[Length] int NOT NULL,
					[Interest] decimal(18, 2) NOT NULL,
                CONSTRAINT [PK_TestObject] PRIMARY KEY CLUSTERED 
                (
	                [Id] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
                ) ON [PRIMARY]
                GO

               CREATE TABLE [dbo].[TestObjectStatus](
	                [ID] [uniqueidentifier] NOT NULL,
	                [TestObjectId] [uniqueidentifier] NOT NULL,
	                [ActiveFromDate] [datetime] NOT NULL,
	                [ActiveToDate] [datetime] NULL,
                    [TestObjectStatusCode] smallint NOT NULL
                CONSTRAINT [PK_TestObjectStatus] PRIMARY KEY CLUSTERED 
                (
	                [Id] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
                ) ON [PRIMARY]
                GO

                ALTER TABLE [dbo].[TestObjectStatus]  WITH CHECK ADD  CONSTRAINT [FK_TestObjectStatus_TestObject] FOREIGN KEY([TestObjectId])
                REFERENCES [dbo].[TestObject] ([ID])
                GO

                ALTER TABLE [dbo].[TestObjectStatus] CHECK CONSTRAINT [FK_TestObjectStatus_TestObject]
                GO


                CREATE PROCEDURE dbo.sp_FillSomeTable(@testObjectId uniqueidentifier)
                AS
                BEGIN

                INSERT INTO [SomeTable](
			                 [TestObjectId]
			                ,[Number]
			                ,[PartyId]
			                ,[UserName]
			                ,[NewCreditCustomer]
			                ,[ApplicationStartedDate]
			                ,[ApplicationSignedHour]
			                ,[ApplicationSignedWeekday]
			                ,[VerificationType]
			                ,[LanguageCode]
			                ,[Age]
			                ,[Gender]
			                ,[Country]
			                ,[AppliedAmount]
			                ,[Interest]
			                ,[Duration]
			                ,[County]
			                ,[City]
			                ,[Education]
			                ,[MaritalStatus]
			                ,[NrOfDependants]
			                ,[EmploymentStatus]
			                ,[EmploymentDurationCurrentEmployer]
			                ,[EmploymentPosition]
			                ,[WorkExperience]
			                ,[OccupationArea]
			                ,[HomeOwnershipType]
			                ,[IncomeFromPrincipalEmployer]
			                ,[IncomeFromPension]
			                ,[IncomeFromFamilyAllowance]
			                ,[IncomeFromSocialWelfare]
			                ,[IncomeFromLeavePay]
			                ,[IncomeFromChildSupport]
			                ,[IncomeOther]
			                ,[IncomeTotal]
			                ,[MonthlyPaymentDay]
			                ,[ModelVersion]
			                ,[ExpectedLoss]
			                ,[Rating]
			                ,[LossGivenDefault]
                            ,[ProbabilityOfDefault]
                            ,[ExpectedReturnAlpha]
			                ,[EADRate]
                            ,[LiabilitiesTotal]
			                ,[ExistingLiabilities]
			                ,[RefinanceLiabilities]
			                ,[CreditScore]
			                ,[DateOfBirth])
                SELECT     r.TestObjectId
                , r.number as LoanNumber
                , r.PartyId
                , r.Username
                , r.NewCreditCustomer
                , r.ApplicationStartedDate
                , datepart(hour, r.ApplicationSubmittedDate) as ApplicationSignedHour
                , datepart(weekday, r.ApplicationSubmittedDate) as ApplicationSignedWeekday
                , r.IncomeAndExpensesVerificationType
                , r.LanguageCode 
                , DATEDIFF(year, r.DateOfBirth, r.ApplicationStartedDate)-IIF(datepart(month, r.ApplicationStartedDate) < datepart(month, r.DateOfBirth) or (datepart(month, r.ApplicationStartedDate) = datepart(month, r.DateOfBirth) and datepart(day, r.ApplicationStartedDate) < datepart(day, r.DateOfBirth)), 1, 0) AS Age
                , r.Gender
                , UPPER(r.Country) as Country
                , t.Amount as AppliedAmount
                , t.Interest as Interest
                , r.Duration
                , r.AddressCounty
                , r.AddressCity
                , r.Education
                , r.MaritalStatus
                , r.NrOfDependants
                , NULLIF(r.EmploymentStatus, 1) as EmploymentStatus
                , r.EmploymentDurationCurrentEmployer
                , r.EmploymentPosition
                , r.WorkExperience
                , r.OccupationArea
                , r.HomeOwnershipType
                , r.IncomeFromPrincipalEmployer
                , r.IncomeFromPension
                , r.IncomeFromFamilyAllowance
                , r.IncomeFromSocialWelfare
                , r.IncomeFromLeavePay
                , r.IncomeFromChildSupport
                , r.IncomeOther
                , r.IncomeTotal
                , r.MonthlyPaymentDay
                , r.ModelVersion
                , r.ExpectedLoss
                , r.Rating
                , r.LossGivenDefault
                , r.ProbabilityOfDefault
                , r.ExpectedReturn
                , r.ExposureAtDefault
                , ISNULL((SELECT SUM(MonthlyPaymentAmount) from dbo.TestObjectRequestLiability where testobjectid = @testObjectId),0) as LiabilitiesTotal
                , (SELECT COUNT(1) AS ExistingLiabilities from dbo.TestObjectRequestLiability where testobjectid = @testObjectId) AS ExistingLiabilities
                , (SELECT COUNT(1) AS RefinanceLiabilities from dbo.TestObjectRequestLiability where testobjectid = @testObjectId	AND WillBeRefinanced = 1) AS RefinanceLiabilities
                , r.CreditScore
                , r.DateOfBirth
                FROM dbo.TestObjectRequest r
                LEFT JOIN dbo.TestObject t ON t.id = r.testobjectid
                WHERE r.testobjectid = @testObjectId

                END
                GO
            ");
        }

        public override void Down()
        {
            Execute.Sql(@"
                USE CompanyDB;
                GO

                drop table [dbo].[TestObject]
                drop table [dbo].[TestObjectStatus]
            ");
        }
    }
}
