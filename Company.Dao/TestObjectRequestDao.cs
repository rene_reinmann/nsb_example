﻿using System;
using Company.DataModel.Entity;
using Company.Persitence.NHibernate;
using NHibernate;

namespace Company.Dao
{
    public class TestObjectRequestDao : GenericDaoWithTypedId<TestObjectRequest, Guid>, ITestObjectRequestDao
    {
        public TestObjectRequestDao(ISession session)
        {
            Session = session;
        }
    }
}