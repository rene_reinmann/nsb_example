﻿using System;
using Company.DataModel.Entity;
using Company.Persitence.NHibernate;

namespace Company.Dao
{
    public interface ITestObjectDao : IDaoWithTypedId<TestObject, Guid>
    {
        void FillTable(Guid testObjectId);
        TestObjectDto FindObjectForSomething(Guid testObjectId);
    }
}