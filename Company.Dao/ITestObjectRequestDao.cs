﻿using System;
using Company.DataModel.Entity;
using Company.Persitence.NHibernate;

namespace Company.Dao
{
    public interface ITestObjectRequestDao : IDaoWithTypedId<TestObjectRequest, Guid>
    {
    }
}