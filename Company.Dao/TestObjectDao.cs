﻿using System;
using System.Linq;
using Company.DataModel.Entity;
using Company.Persitence.NHibernate;
using NHibernate;
using NHibernate.Transform;

namespace Company.Dao
{
    public class TestObjectDao : GenericDaoWithTypedId<TestObject, Guid>, ITestObjectDao
    {
        public TestObjectDao(ISession session)
        {
            Session = session;
        }

        public void FillTable(Guid testObjectId)
        {
            IQuery query = Session.CreateSQLQuery("exec dbo.sp_FillSomeTable @TestObjectId = :testObjectId");
            query.SetGuid("testObjectId", testObjectId);
            query.ExecuteUpdate();
        }

        public TestObjectDto FindObjectForSomething(Guid testObjectId)
        {
            const string sql = @"
    SELECT 
        t.Id, 
	    t.CreatedOn,
	    t.Country,
	    t.PartyId,
        t.Amount,
        convert(decimal(18, 2), ROUND(s.ExpectedReturnAlpha * 100, 2)) AS ExpectedReturnAlpha,
        convert(decimal(18, 2), ROUND(s.ExpectedLoss * 100, 2)) AS ExpectedLoss
    FROM dbo.TestObject t
    INNER JOIN dbo.SomeTable s on s.TestObjectId = t.Id
    WHERE t.ID = :testObjectId
";
            var query = Session.CreateSQLQuery(sql);

            query.SetGuid("testObjectId", testObjectId);
            query.SetResultTransformer(Transformers.AliasToBean<TestObjectDto>());

            return query.List<TestObjectDto>().FirstOrDefault();
        }
    }

    public class TestObjectDto
    {
        public Guid Id { get; set; }
        public Guid PartyId { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal Amount { get; set; }
        public string Country { get; set; }
        public decimal ExpectedReturnAlpha { get; set; }
        public decimal ExpectedLoss { get; set; }
    }
}
