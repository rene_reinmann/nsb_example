﻿using System.Threading.Tasks;
using Company.NSB.Messages.EndpointA;
using NServiceBus;

namespace Company.NSB.Services.EndpointA
{
    public class StartTestCommandHandler : IHandleMessages<StartTestCommand>
    {
        public async Task Handle(StartTestCommand message, IMessageHandlerContext context)
        {
            for (var i = 1; i <= message.Number; i++) 
                await context.SendLocal(new RequestTestObjectCommand());
        }
    }
}
