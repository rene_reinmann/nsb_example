﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Company.Dao;
using Company.DataModel.Entity;
using Company.NSB.Messages.EndpointA;
using Company.NSB.Messages.EndpointB;
using NServiceBus;

namespace Company.NSB.Services.EndpointA
{
    public class RequestTestObjectCommandHandler : IHandleMessages<RequestTestObjectCommand>
    {
        private static Random random = new Random(int.MaxValue);

        public ITestObjectDao TestObjectDao { get; set; }
        public ITestObjectRequestDao TestObjectRequestDao { get; set; }

        public async Task Handle(RequestTestObjectCommand message, IMessageHandlerContext context)
        {
            var testObject = SaveTestObjectRequest();

            DoSomething(testObject);

            await SendCommands(context, testObject);

            //Console.WriteLine(DateTime.Now + ": Message RequestTestObjectCommand handled");
        }

        private TestObjectRequest SaveTestObjectRequest()
        {
            var testObjectRequest = GetRandomRequest();

            TestObjectRequestDao.Save(testObjectRequest);
            TestObjectRequestDao.CommitChanges();

            return testObjectRequest;
        }

        private void DoSomething(TestObjectRequest testObjectRequest)
        {
            var testObject = new TestObject(testObjectRequest);

            testObject.ChangeStatusTo(TestObjectStatusCode.Open);

            //Save obj
            TestObjectDao.Save(testObject);
            TestObjectDao.CommitChanges();

            //Fill extra table with stored proc
            TestObjectDao.FillTable(testObject.ID);
        }

        private static async Task SendCommands(IMessageHandlerContext context, TestObjectRequest testObject)
        {
            await context.Send(new TestObjectCreatedCommand
            {
                TestObjectId = testObject.ID
            });
        }

        private static TestObjectRequest GetRandomRequest()
        {
            var randomRequest = new TestObjectRequest
            {
                ApplicationStartedDate = DateTime.Now,
                ApplicationSubmittedDate = DateTime.Now,
                Number = 1,
                AppliedAmount = random.Next(500, 10000),
                Interest = random.Next(9, 60),
                Country = "EE",
                Duration = 60,
                MonthlyPaymentDay = 10,
                MonthlyPayment = 100,

                AnnualPercentageRate = 25,
                PartyId = Guid.NewGuid(),
                Username = RandomString(8),
                ReferralNr = "12345",
                AddressCountry = "Estonia",
                AddressCounty = "Harju",
                AddressCity = "Talinn",
                DateOfBirth = new DateTime(random.Next(1950, 1990), random.Next(1, 12), random.Next(1, 27)),
                Education = -1,
                EmploymentDurationCurrentEmployer = "",

                Gender = random.Next(0, 2),
                HomeOwnershipType = -1,
                LanguageCode = -1,
                IncomeAndExpensesVerificationType = -1,

                IncomeTotal = 1200,
                NewCreditCustomer = true,
                CreditScore = "1000",
                Rating = RandomString(1),
                ModelVersion = "1",
                ExpectedLoss = random.NextDouble(),
                ExpectedReturn = random.NextDouble(),
                LossGivenDefault = random.NextDouble(),
                ExposureAtDefault = random.NextDouble(),
                ProbabilityOfDefault = random.NextDouble(),
                ProbabilityOfBad = random.NextDouble(),

                DebtToIncome = 0,
                FreeCash = 0,
                MaritalStatus = -1,
                EmploymentPosition = null,
                EmploymentStatus = -1,
                OccupationArea = -1,
                WorkExperience = null,
                NrOfDependants = null,
                IncomeFromChildSupport = 0,
                IncomeFromFamilyAllowance = 0,
                IncomeFromLeavePay = 0,
                IncomeFromPrincipalEmployer = 0,
                IncomeFromPension = 0,
                IncomeFromSocialWelfare = 0,
                IncomeOther = 0,
                MonthlyLivingExpenses = 0
            };

            randomRequest.SetAssignedIdTo(Guid.NewGuid());
            
            return randomRequest;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
