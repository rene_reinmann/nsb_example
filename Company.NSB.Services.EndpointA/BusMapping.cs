﻿
using Company.NSB.Common;
using Company.NSB.Messages;
using Company.NSB.Messages.EndpointB;

namespace Company.NSB.Services.EndpointA
{
    public class BusMapping : NsbLogicalRouting
    {
        public BusMapping()
        {
            To(EndpointName.EndpointB)
                .Send<TestObjectCreatedCommand>();
        }
    }
}