﻿using System;
using Company.NSB.Host;

namespace Company.NSB.Services.EndpointA
{
    class ServiceHost : BaseServiceHost<ServiceHost, BusMapping>
    {
        private static readonly IServiceConfig Config = new ServiceConfig
        {
            MessageProcessingConcurrency = 1,
            ImmediateNumberOfRetries = 1,
            DelayedNumberOfRetries = 2,
            DelayedRetryTimeIncrease = TimeSpan.FromSeconds(60)
        };

        private static void Main()
        {
            new ServiceHost().Start();
        }

        public ServiceHost() : base(Config, new ServiceBootstrapper())
        {
        }
    }
}