﻿using System;

namespace Company.NSB.Messages
{
    [Serializable]
    public class BaseResponse : IBaseResponse
    {
        public Guid ResponseId { get; set; }
        public Guid ToMessageId { get; set; }
        public DateTime OccuredOn { get; set; }
        public bool Success { get; set; }

        public BaseResponse()
        {
            ResponseId = Guid.NewGuid();
            OccuredOn = DateTime.Now;
        }
    }
}