﻿namespace Company.NSB.Messages.EndpointA
{
    public class StartTestCommand : BaseCommand
    {
        public int Number { get; set; }
    }
}