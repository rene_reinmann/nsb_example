﻿using System;

namespace Company.NSB.Messages
{
    public interface IBaseMessage
    {
        Guid MessageId { get; set; }
        DateTime OccuredOn { get; set; }
        string Creator { get; set; }
        string Source { get; set; }
    }
}