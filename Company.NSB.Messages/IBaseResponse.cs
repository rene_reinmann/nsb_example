﻿using System;

namespace Company.NSB.Messages
{
    public interface IBaseResponse
    {
        Guid ResponseId { get; set; }
        Guid ToMessageId { get; set; }
        DateTime OccuredOn { get; set; }
        bool Success { get; set; }
    }
}