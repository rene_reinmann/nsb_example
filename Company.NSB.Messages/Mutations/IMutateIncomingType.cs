﻿namespace Company.NSB.Messages.Mutations
{
    public interface IMutateIncomingType
    {
        string FromAssembly { get; }
        string FromNamespace { get; }
        string ToAssembly { get; }
        string ToNamespace { get; }
    }
}