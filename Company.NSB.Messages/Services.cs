﻿using Company.NSB.Common;

namespace Company.NSB.Messages
{
    /*
     * Services starting with Company*
     * Note that underscore _ gets replaced by period '.'
    */
    [Queue("company.nsb.services")]
    public enum EndpointName
    {
        EndpointA,
        EndpointB,
    }
}