﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Company.NSB.Messages
{
    [Serializable]
    public class BaseCommand : IBaseMessage
    {
        protected BaseCommand()
        {
            MessageId = Guid.NewGuid();
            OccuredOn = DateTime.Now;
            Source = string.Empty;
            Creator = GetDefaultCreator();
        }

        public Guid MessageId { get; set; }
        public DateTime OccuredOn { get; set; }

        /// <summary>
        ///     Creator will is automatically set to the name of the method where the message is created. You can still set your
        ///     own text (it will overwrite the default).
        /// </summary>
        public string Creator { get; set; }

        public string Source { get; set; }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static string GetDefaultCreator()
        {
            for (int skip = 1; ; skip++)
            {
                var stackFrame = new StackFrame(skip, false);
                MethodBase method = stackFrame.GetMethod();
                if (method.Name == ".ctor")
                    continue;

                Type type = method.DeclaringType;
                string typeName = type == null ? "Global" : type.Name;

                if (method.Name == ".ctor" || method.Name == "MoveNext" ||
                    (typeName == "AsyncTaskMethodBuilder" && method.Name == "Start") ||
                    (typeName == "RuntimeMethodHandle" && method.Name == "InvokeMethod") ||
                    (typeName == "ExecutionContext" && (method.Name == "Run" || method.Name == "RunInternal")) ||
                    (typeName == "RuntimeConstructorInfo" && method.Name == "Invoke"))
                    continue;

                return typeName + "." + method.Name;
            }
        }
    }
}