﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Company.NSB.Messages
{
    [Serializable]
    public class BaseEvent : IBaseMessage
    {
        public BaseEvent()
        {
            Creator = GetDefaultCreator();
            MessageId = Guid.NewGuid();
            OccuredOn = DateTime.Now;
            Source = string.Empty;
        }

        public Guid MessageId { get; set; }
        public DateTime OccuredOn { get; set; }
        public string Creator { get; set; }
        public string Source { get; set; }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static string GetDefaultCreator()
        {
            for (int skip = 1; ; skip++)
            {
                var stackFrame = new StackFrame(skip, false);
                MethodBase method = stackFrame.GetMethod();
                if (method.Name == ".ctor")
                    continue;

                Type type = method.DeclaringType;
                string typeName = type == null ? "Global" : type.Name;

                return typeName + "." + method.Name;
            }
        }
    }
}