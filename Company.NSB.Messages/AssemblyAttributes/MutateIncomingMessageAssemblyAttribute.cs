﻿using System;
using Company.NSB.Messages.Mutations;

namespace Company.NSB.Messages.AssemblyAttributes
{
    /// <summary>
    /// Use this assembly attribute to define assembly and optionally namespace mutation for incoming message.
    /// Use it together with <see cref="MessageAssemblyAttribute"/> attribute, so that it can be automatically discovered.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly)]
    public class MutateIncomingMessageAssemblyAttribute : Attribute, IMutateIncomingType
    {
        public string FromAssembly { get; }
        public string FromNamespace { get; }
        public string ToAssembly { get; }
        public string ToNamespace { get; }

        public MutateIncomingMessageAssemblyAttribute(string fromAssembly, string fromNamespace = null, string toAssembly = null, string toNamespace = null)
        {
            if (string.IsNullOrEmpty(fromAssembly))
                throw new ArgumentException(nameof(fromAssembly));

            FromAssembly = fromAssembly;
            FromNamespace = fromNamespace ?? FromAssembly;
            ToAssembly = toAssembly ?? System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            ToNamespace = toNamespace ?? ToAssembly;
        }
    }
}