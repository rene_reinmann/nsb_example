﻿using System;

namespace Company.NSB.Messages.AssemblyAttributes
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class MessageAssemblyAttribute : Attribute
    {
    }
}