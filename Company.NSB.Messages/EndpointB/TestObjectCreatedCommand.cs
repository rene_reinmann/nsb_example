﻿using System;

namespace Company.NSB.Messages.EndpointB
{
    public class TestObjectCreatedCommand : BaseCommand
    {
        public Guid TestObjectId { get; set; }
    }
}