﻿using System;
using System.Linq;

namespace Company.NSB.Common
{
    /// <summary>
    ///     Sets up event origination endpoint.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class PublishedByAttribute : Attribute
    {
        public Enum Endpoint { get; }
        public string EndpointName => Endpoint.ToString();

        public PublishedByAttribute(object enumValue)
        {
            if (!(enumValue is Enum))
                throw new ArgumentException($"{nameof(enumValue)} must be a Enum type");

            Endpoint = (Enum)enumValue;
        }

        public string GetQueueName()
        {
            var queueNameAttribute = Endpoint.GetType().GetField(EndpointName)
                .GetCustomAttributes(typeof(EndpointAttribute), true).FirstOrDefault();
            return queueNameAttribute != null ? ((EndpointAttribute)queueNameAttribute).QueueName : null;
        }
    }
}