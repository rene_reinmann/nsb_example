﻿using System;

namespace Company.NSB.Common
{
    public interface IMessageRouting
    {
        Type MessageType { get; }
        Enum Endpoint { get; }
        int Priority { get; }
    }
}