﻿using System;

namespace Company.NSB.Common
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum)]
    public class QueueAttribute : Attribute
    {
        public string Queue { get; set; }

        public QueueAttribute(string queue)
        {
            Queue = queue;
        }
    }
}