﻿using System;

namespace Company.NSB.Common
{
    [AttributeUsage(AttributeTargets.Field)]
    public class EndpointAttribute : Attribute
    {
        public EndpointAttribute(string queueName)
        {
            QueueName = queueName;
        }

        public string QueueName { get; }
    }
}