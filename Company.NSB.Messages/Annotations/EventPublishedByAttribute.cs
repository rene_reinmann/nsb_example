﻿using System;

namespace Company.NSB.Common
{
    /// <summary>
    ///     Sets up event origination endpoint.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class EventPublishedByAttribute : PublishedByAttribute, IMessageRouting
    {
        public Type MessageType { get; }
        public int Priority { get; set; }

        public EventPublishedByAttribute(Type messageType, object enumValue, int priority = 0) : base(enumValue)
        {
            if (messageType == null)
                throw new ArgumentException(nameof(messageType));

            MessageType = messageType;
            Priority = priority;
        }
    }
}