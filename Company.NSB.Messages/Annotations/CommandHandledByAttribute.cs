﻿using System;

namespace Company.NSB.Common
{
    /// <summary>
    ///     Sets up default message destination endpoint. 
    ///     If not overridden in BusMapping then the message will be send to specified service queue.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class CommandHandledByAttribute : HandledByAttribute, IMessageRouting
    {
        public Type MessageType { get; }
        public int Priority { get; set; }

        public CommandHandledByAttribute(Type messageType, object enumValue, int priority = 0) : base(enumValue)
        {
            if (messageType == null)
                throw new ArgumentException(nameof(messageType));

            MessageType = messageType;
            Priority = priority;
        }
    }
}