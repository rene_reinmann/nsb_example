﻿using System;
using System.Linq;

namespace Company.NSB.Common
{
    /// <summary>
    ///     Sets up default message destination endpoint. 
    ///     If not overridden in BusMapping then the message will be send to specified service queue.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class HandledByAttribute : Attribute
    {
        public Enum Endpoint { get; }
        public string EndpointName => Endpoint.ToString();

        public HandledByAttribute(object enumValue)
        {
            if (!(enumValue is Enum))
                throw new ArgumentException($"{nameof(enumValue)} must be a Enum type");

            Endpoint = (Enum)enumValue;
        }

        public string GetQueueName()
        {
            var queueNameAttribute = Endpoint.GetType().GetField(EndpointName)
                .GetCustomAttributes(typeof(EndpointAttribute), true).FirstOrDefault();
            return queueNameAttribute != null ? ((EndpointAttribute)queueNameAttribute).QueueName : null;
        }
    }
}