﻿using System;
using System.Collections.Specialized;

namespace Company.Config
{
    public class App
    {
        private static NameValueCollection AppSettings => System.Configuration.ConfigurationManager.AppSettings;
        private static string GetBuildConfig => AppSettings["BuildConfig"];
        public static string NsbEndpointName => AppSettings["NsbEndpointName"];

        public static bool IsBuildConfigDebug => GetBuildConfig.Equals("debug", StringComparison.InvariantCultureIgnoreCase);

    }
}
