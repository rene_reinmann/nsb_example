﻿using Autofac;
using Company.NSB.Common;
using Company.NSB.Host;

namespace Company.NSB.Services.EndpointB
{
    public class ServiceBootstrapper : IServiceBootstrapper
    {
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<RegisterNHibernateModule>();
            builder.RegisterDaoTypes();
        }
    }
}