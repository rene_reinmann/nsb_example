﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Company.Dao;
using Company.NSB.Messages.EndpointB;
using log4net;
using NServiceBus;

namespace Company.NSB.Services.EndpointB
{
    public class TestObjectCreatedCommandHandler : IHandleMessages<TestObjectCreatedCommand>
    {
        public ITestObjectDao TestObjectDao { get; set; }
        public ILog Log { get; set; }

        public TestObjectCreatedCommandHandler()
        {
            Log = LogManager.GetLogger("CustomTrace");
        }

        public Task Handle(TestObjectCreatedCommand message, IMessageHandlerContext context)
        {
            //Get Object
            var obj = TestObjectDao.FindObjectForSomething(message.TestObjectId);
            if (obj == null)
            {
                var errorMessage = $"Obj was not found: {message.TestObjectId}";

                Console.WriteLine(errorMessage);
                throw new ApplicationException(errorMessage);
            }

            //Console.WriteLine(DateTime.Now + ": Message TestObjectCreatedCommand handled");
            return Task.CompletedTask;
        }
    }
}
