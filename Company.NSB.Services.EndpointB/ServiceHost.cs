﻿using System;
using System.Collections.Generic;
using Company.NSB.Host;

namespace Company.NSB.Services.EndpointB
{
    class ServiceHost : BaseServiceHost<ServiceHost, BusMapping>
    {
        private static readonly IServiceConfig Config = new ServiceConfig
        {
            MessageProcessingConcurrency = 3,
            ImmediateNumberOfRetries = 2,
            DelayedNumberOfRetries = 1,
            DelayedRetryTimeIncrease = TimeSpan.FromSeconds(60)
        };

        private static void Main()
        {
            new ServiceHost().Start();
        }

        public ServiceHost() : base(Config, new ServiceBootstrapper())
        {
        }

        protected override void ConfigureLogging(bool isDebug, Dictionary<string, string> customLogLevels)
        {
            customLogLevels.Add("CustomTrace", "INFO");

            base.ConfigureLogging(isDebug, customLogLevels);
        }
    }
}