﻿using Autofac;

namespace Company.NSB.Host
{
    public interface IServiceBootstrapper
    {
        void ConfigureContainer(ContainerBuilder builder);
    }
}
