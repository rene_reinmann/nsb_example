﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company.NSB.Host
{
    public interface IServiceHost
    {
        void Start();
        void Stop();
    }

}
