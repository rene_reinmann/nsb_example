﻿using Autofac;

namespace Company.NSB.Host
{
    public interface IServiceBootstrapperWithContainer : IServiceBootstrapper
    {
        void SetContainer(ILifetimeScope container);
    }
}