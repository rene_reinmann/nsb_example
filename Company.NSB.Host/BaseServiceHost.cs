﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading.Tasks;
using Autofac;
using Company.Common.Logging;
using Company.Config;
using Company.NSB.Common;
using log4net;
using NServiceBus;


namespace Company.NSB.Host
{
    [DesignerCategory("Code")]
    public abstract class BaseServiceHost<TEndpoint, TMapping> : ServiceBase, IServiceHost where TMapping : NsbLogicalRouting, new()
    {
        private readonly ILog _log;
        private readonly IServiceConfig _config;
        private readonly IServiceBootstrapper _bootstrapper;
        
        private IEndpointInstance _endpoint;

        protected internal bool RunningInTestMode { get; set; }

        protected BaseServiceHost(IServiceConfig config = null, IServiceBootstrapper bootstrapper = null)
        {
            if (config == null)
                config = new ServiceConfig();

            _config = config;
            _bootstrapper = bootstrapper;

            _log = LogManager.GetLogger(typeof(TEndpoint));

            ConfigureLogging();
        }

        /// <summary>
        /// Starts the Service
        /// </summary>
        public void Start()
        {
            using (var service = this)
            {
                // to run interactive from a console or as a windows service
                if (RunningInTestMode || Environment.UserInteractive)
                {
                    RunAsConsoleApp(service);
                    return;
                }
                Run(service);
            }
        }

        private void RunAsConsoleApp(BaseServiceHost<TEndpoint, TMapping> service)
        {
            if (!RunningInTestMode && !Console.IsOutputRedirected)
            {
                Console.Title = typeof(TEndpoint).Namespace ?? "BaseServiceHost";
                Console.CancelKeyPress += (sender, e) =>
                {
                    service.OnStop();
                };
            }

            service.OnStart(null);

            if (RunningInTestMode)
                return;

            Console.WriteLine("\r\nPress enter key to stop program\r\n");
            Console.Read();
            service.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            OnStartAsync().GetAwaiter().GetResult();
        }

        private async Task OnStartAsync()
        {
            try
            {
                if (string.IsNullOrEmpty(_config.EndpointName))
                    _config.EndpointName = typeof(TEndpoint).Namespace;

                var container = await ConfigureEndpoint().ConfigureAwait(false);

                await OnEndpointStart(_endpoint).ConfigureAwait(false);

                await PerformStartupOperations(container).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                _log.Fatal("Failed to start", exception);

                if (RunningInTestMode || Debugger.IsAttached)
                    throw;

                Environment.FailFast("Failed to start", exception);
            }
        }

        private async Task<ILifetimeScope> ConfigureEndpoint()
        {
            var endpointConfiguration = new EndpointConfiguration(_config.EndpointName);
            endpointConfiguration.UseDefaultConfiguration<TEndpoint, TMapping>(_config);
            endpointConfiguration.DefineCriticalErrorAction(OnCriticalError);

            ILifetimeScope container = endpointConfiguration.ConfigureContainer(ConfigureContainer);

            var bootstrapper = _bootstrapper as IServiceBootstrapperWithContainer;
            bootstrapper?.SetContainer(container);
            
            PreserveFailedQueueHeaders(endpointConfiguration);
            ConfigureEndpoint(endpointConfiguration, container);

            _endpoint = await StartEndpoint(endpointConfiguration).ConfigureAwait(false);

            return container;
        }

        private static void PreserveFailedQueueHeaders(EndpointConfiguration endpointConfiguration)
        {
            RecoverabilitySettings recoverability = endpointConfiguration.Recoverability();
            recoverability.Failed(
                failed =>
                {
                    failed.HeaderCustomization(
                        headers =>
                        {
                            if (headers.ContainsKey("NServiceBus.FailedQ")
                                    && !headers.ContainsKey("NServiceBus.OriginalFailedQ"))
                                headers["NServiceBus.OriginalFailedQ"] = headers["NServiceBus.FailedQ"];
                        });
                });
        }

        /// <summary>
        /// Configure your Endpoint additional settings here.
        /// </summary>
        /// <example>
        /// For example enabling callbacks: <code>endpointConfiguration.EnableCallbacks(makesRequests: false);</code>
        /// </example>
        /// <param name="endpointConfiguration"></param>
        protected virtual void ConfigureEndpoint(EndpointConfiguration endpointConfiguration)
        {
        }

        /// <summary>Configure your Endpoint additional settings here.</summary>
        /// <example>For example enabling callbacks:
        /// <code>endpointConfiguration.EnableCallbacks(makesRequests: false);</code>
        /// </example>
        /// <param name="endpointConfiguration"></param>
        /// <param name="container">The container.</param>
        protected virtual void ConfigureEndpoint(EndpointConfiguration endpointConfiguration, ILifetimeScope container)
        {
            ConfigureEndpoint(endpointConfiguration);
        }

        /// <summary>
        /// For internal use only. DO NOT use directly from code!
        /// </summary>
        /// <param name="endpointConfiguration"></param>
        /// <returns>Started Endpoint instance</returns>
        internal virtual Task<IEndpointInstance> StartEndpoint(EndpointConfiguration endpointConfiguration)
        {
            return Endpoint.Start(endpointConfiguration);
        }

        private ContainerBuilder ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterInstance<IServiceHost>(this).SingleInstance().ExternallyOwned();
            builder.RegisterInstance<IServiceConfig>(_config).SingleInstance();

            _bootstrapper?.ConfigureContainer(builder);

            return builder;
        }

        /// <summary>
        /// Called after the <see cref="OnEndpointStart"/> method.
        /// By default this stats the ApiHost.
        /// </summary>
        /// <param name="container">Autofac container</param>
        /// <returns>Async task</returns>
        protected virtual Task PerformStartupOperations(ILifetimeScope container)
        {
            return Task.FromResult(0);
        }

        /// <summary>
        /// Called on critical NServiceBus error.
        /// By default it exists the service process with failure message.
        /// Override to perform additional checks and decide if to exit the process or keep running.
        /// </summary>
        /// <param name="context">Error context</param>
        /// <returns>Async task</returns>
        protected virtual Task OnCriticalError(ICriticalErrorContext context)
        {
            //TODO: Decide if shutting down the process is the best response to a critical error
            // https://docs.particular.net/nservicebus/hosting/critical-errors
            var fatalMessage =
                $"The following critical error was encountered:\n{context.Error}\nProcess is shutting down.";
            _log.Fatal(fatalMessage, context.Exception);
            Environment.FailFast(fatalMessage, context.Exception);
            return Task.FromResult(0);
        }

        protected override void OnStop()
        {
            OnStopAsync().GetAwaiter().GetResult();

            if (!RunningInTestMode && Environment.UserInteractive)
                Environment.Exit(0);
        }

        private async Task OnStopAsync()
        {
            await OnEndpointStop(_endpoint).ConfigureAwait(false);

            if (_endpoint != null)
                await _endpoint.Stop().ConfigureAwait(false);
        }

        #region Endpoint Start/Stop

        /// <summary>
        /// Override to perform tasks on Endpoint start
        /// </summary>
        /// <param name="session">Message session that can be used to send or publish messages</param>
        /// <returns>Async task</returns>
        protected virtual Task OnEndpointStart(IMessageSession session)
        {
            return Task.FromResult(0);
        }
        
        /// <summary>
        /// Override to perform tasks on Endpoint stop
        /// </summary>
        /// <param name="session">Message session that can be used to send or publish messages</param>
        /// <returns>Async task</returns>
        protected virtual Task OnEndpointStop(IMessageSession session)
        {
            return Task.FromResult(0);
        }

        #endregion

        #region Logging

        private void ConfigureLogging()
        {
            bool isDebug = Debugger.IsAttached || App.IsBuildConfigDebug;
            var customLogLevels = new Dictionary<string, string>{
                { "NHibernate", isDebug ? "WARN" : "FATAL"},
                { "NHibernate.SQL", isDebug ? "WARN" : "FATAL"}
            };

            ConfigureLogging(isDebug, customLogLevels);

            NServiceBus.Logging.LogManager.Use<Log4NetFactory>();
        }

        /// <summary>
        /// Configures logging and custom log levels.
        /// Override to use your own custom log levels.
        /// </summary>
        /// <param name="isDebug">If the service is running in Debug</param>
        /// <param name="customLogLevels">Log levels in dictionary - key: LoggerName, value: Log level. (Ex. { "NHibernate", "WARN" }) </param>
        protected virtual void ConfigureLogging(bool isDebug, Dictionary<string, string> customLogLevels)
        {
            ConfigureLog4Net.Programmatically(isDebug, customLogLevels);
        }

        #endregion

    }
}