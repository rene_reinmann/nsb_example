﻿using System;
using System.Collections.Generic;

namespace Company.NSB.Host
{
    public class ServiceConfig : IServiceConfig
    {
        public string EndpointName { get; set; }
        public string EndpointQueue { get; set; }
        public string AuditQueue { get; set; }
        public string ErrorQueue { get; set; }
        public string HeartbeatQueue { get; set; }
        public TimeSpan HeartbeatInterval { get; set; }
        public TimeSpan RaiseHeartbeatAlertAfter { get; set; }
        public bool EnableInstallers { get; set; }
        public bool MultithreadingEnabled { get; set; }
        public bool DisableDTC { get; set; }
        public bool DisableAutoSubscribe { get; set; }
        public int MessageProcessingConcurrency { get; set; }
        public int ImmediateNumberOfRetries { get; set; }
        public int DelayedNumberOfRetries { get; set; }
        public TimeSpan DelayedRetryTimeIncrease { get; set; }
        public List<string> Routing { get; set; }
        public string MachineName { get; set; }
        public bool UseV4ToV6MessageMutator { get; set; }
        public bool UseV6ToV4MessageMutator { get; set; }
        public bool UseJsonSerializer { get; set; }
        public bool EnableCallbacks { get; set; }
        public bool CallbacksMakeRequests { get; set; }
        public bool SendOnly { get; set; }
        public bool StoreCurrentMessages { get; set; }

        public ServiceConfig()
        {
            EnableInstallers = true;
            MultithreadingEnabled = true;
            DisableDTC = false;
            DisableAutoSubscribe = false;
            AuditQueue = "company.nsb.services.eventlog";
            ErrorQueue = "company.nsb.services.eventerrorlog";
            RaiseHeartbeatAlertAfter = TimeSpan.Zero;
            HeartbeatInterval = TimeSpan.Zero;
            MessageProcessingConcurrency = Math.Max(2, Environment.ProcessorCount);
            ImmediateNumberOfRetries = 3;
            DelayedNumberOfRetries = 3;
            DelayedRetryTimeIncrease = TimeSpan.FromSeconds(10);
            MachineName = Environment.MachineName;
            UseV4ToV6MessageMutator = false;
            UseV6ToV4MessageMutator = false;
            StoreCurrentMessages = false;
            UseJsonSerializer = true;
        }
    }
}