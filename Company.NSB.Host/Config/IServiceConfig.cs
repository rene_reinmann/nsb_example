﻿using System;

namespace Company.NSB.Host
{
    public interface IServiceConfig : Company.NSB.Common.IEndpointConfig
    {
        TimeSpan HeartbeatInterval { get; set; }
        TimeSpan RaiseHeartbeatAlertAfter { get; set; }
        string MachineName { get; set; }
        bool StoreCurrentMessages { get; set; }
    }

}